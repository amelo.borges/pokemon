# Pokemon

## Teste App Pokemon

No desenvolvimento do meu projeto Flutter, que consome a API (https://pokeapi.co/), decidi realizar uma mudança significativa na gestão de dados: substituir o banco de dados Hive pelo Isar.

Essa decisão foi baseada em várias características superiores do Isar. Primeiramente, o Isar se destaca pela sua alta performance, sendo mais eficiente que o Hive em diversas operações de banco de dados. Essa melhoria no desempenho é crucial para garantir uma experiência de usuário mais fluida e responsiva.

Além disso, o Isar oferece funcionalidades avançadas que não estão disponíveis no Hive. Uma dessas funcionalidades é a capacidade de realizar consultas complexas (Queries) diretamente no banco de dados. Isso proporciona uma maior flexibilidade e eficiência na manipulação de dados, permitindo consultas mais sofisticadas e precisas.

Outro ponto forte do Isar é o suporte para multi-isolamento (multi-threading), o que significa que ele pode gerenciar várias operações em diferentes threads sem comprometer o desempenho. Isso é especialmente benéfico em aplicativos que requerem alto processamento de dados e operações simultâneas.

Por fim, o Isar permite o link entre objetos, facilitando o gerenciamento de relações entre diferentes conjuntos de dados. Isso simplifica a estruturação e a recuperação de informações complexas, tornando o desenvolvimento mais ágil e menos propenso a erros.

Acredito que a transição para o Isar seja uma decisão acertada, alinhando-se com as necessidades de performance e eficiência do meu projeto, ao mesmo tempo que amplia as possibilidades de manipulação de dados de forma mais eficaz e sofisticada.


### Arquitetura  
Clean Architecture
### Gerenciamento de Estado
Getx (https://pub.dev/packages/get)
### Banco de Dados 
Isar (https://pub.dev/packages/isar)
### HTTP networking
Dio (https://pub.dev/packages/dio)