import '../../../domain/entity/move/move_type_entity.dart';

class MoveTypeModel extends MoveTypeEntity{
  String move;
  String type;

  MoveTypeModel({
    required this.move,
    required this.type,
  }) : super(move: move, type: type);

}