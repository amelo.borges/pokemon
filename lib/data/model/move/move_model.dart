import '../../../domain/entity/move/move_entity.dart';
import 'Move_type_model.dart';

class MoveModel extends MoveEntity{
  int id;
  String name;
  List<MoveTypeModel> moveTypesModel;

  MoveModel(
      {
        required this.id,
        required this.name,
        required this.moveTypesModel,
      }) : super(id: id, name: name, moveTypes: moveTypesModel);

}