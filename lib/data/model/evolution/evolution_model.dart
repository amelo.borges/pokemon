import '../../../domain/entity/evolution/evolution_entity.dart';
import 'evolution_type_model.dart';

class EvolutionModel extends EvolutionEntity {
  int id;
  String name;
  List<EvolutionTypeModel> evolutionTypesModel;

  EvolutionModel({
    required this.id,
    required this.name,
    required this.evolutionTypesModel,
  }) : super(id: id, name: name, evolutionTypes: evolutionTypesModel);
}