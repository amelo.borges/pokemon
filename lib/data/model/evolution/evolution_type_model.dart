import '../../../domain/entity/evolution/evolution_type_entity.dart';

class EvolutionTypeModel extends EvolutionTypeEntity{
  String name;
  String url;
  String type;

  EvolutionTypeModel({
    required this.name,
    required this.url,
    required this.type,
  }) : super(name: name, url: url, type: type);

}