import '../../../domain/entity/pokemon/weakness_entity.dart';

class WeaknessModel extends WeaknessEntity {
  String name;
  String url;

  WeaknessModel({
   required this.name,
    required this.url
  }) : super(name: name, url: url);
}