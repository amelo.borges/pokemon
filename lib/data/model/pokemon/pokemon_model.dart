import 'package:pokemon/data/model/pokemon/strenghts_model.dart';
import '../../../domain/entity/pokemon/pokemon_entity.dart';
import 'stats_model.dart';
import 'type_model.dart';
import 'weakness_model.dart';

class PokemonModel extends PokemonEntity {
  int id;
  String name;
  String image;
  String color;
  bool isFavorite;
  int pageGroup;
  String height;
  List<String> abilities;
  List<TypeModel> typesModel;
  String weight;
  List<WeaknessModel> weaknessModel;
  List<StrenghtsModel> strenghtsModel;
  List<StatsModel> statsModel;
  int baseExperience;


  PokemonModel({
    required this.id,
    required this.name,
    required this.image,
    required this.typesModel,
    required this.color,
    required this.pageGroup,
    required this.height,
    required this.abilities,
    required this.weight,
    required this.weaknessModel,
    required this.strenghtsModel,
    required this.statsModel,
    required this.isFavorite,
    required this.baseExperience,
  }) : super(id: id, name: name, image: image, types: typesModel, color: color, isFavorite: isFavorite, pageGroup: pageGroup, height: height, abilities: abilities, weight: weight, weakness: weaknessModel, strenghts: strenghtsModel, stats: statsModel, baseExperience: baseExperience);

}