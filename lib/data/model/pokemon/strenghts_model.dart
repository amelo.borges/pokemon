import '../../../domain/entity/pokemon/strenghts_entity.dart';

class StrenghtsModel extends StrenghtsEntity {
  String name;
  String url;

  StrenghtsModel({
    required this.name,
    required this.url
  }) : super(name: name, url: url);

}