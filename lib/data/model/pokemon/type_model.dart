import 'package:flutter/material.dart';

import '../../../domain/entity/pokemon/type_entity.dart';

class TypeModel extends TypeEntity{
  final String name;
  final String url;
  final String color;
  final String image;

  TypeModel({
    required this.name,
    required this.url,
    required this.color,
    required this.image,
  }) : super(name: name, url: url, color: color, image: image);

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'url': url,
      'color': color,
      'image': image,
    };
  }

}