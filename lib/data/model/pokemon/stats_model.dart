
import '../../../domain/entity/pokemon/stats_entity.dart';

class StatsModel extends StatsEntity{
  int baseStat;
  String name;
  String url;

  StatsModel({
    required this.baseStat,
    required this.name,
    required this.url,
  }) : super(baseStat: baseStat, name: name, url: url);
}
