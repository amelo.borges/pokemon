import 'package:pokemon/core/database/evolution/evolution_collection.dart';
import 'package:pokemon/core/database/move/move_collection.dart';
import 'package:pokemon/data/model/evolution/evolution_type_model.dart';
import 'package:pokemon/data/model/move/Move_type_model.dart';
import 'package:pokemon/data/model/move/move_model.dart';
import 'package:pokemon/data/model/pokemon/strenghts_model.dart';
import 'package:pokemon/data/model/pokemon/weakness_model.dart';
import '../../core/database/pokemon/pokemon_collection.dart';
import '../model/evolution/evolution_model.dart';
import '../model/pokemon/pokemon_model.dart';
import '../model/pokemon/stats_model.dart';
import '../model/pokemon/type_model.dart';

class Mapping {
  static List<PokemonModel> mapToPokemonModel(List<PokemonCollection> items) {
    return items.map((item) => PokemonModel(
      id: item.id,
      name: item.name,
      image: item.image,
      typesModel: item.types.map((type) => TypeModel(
        name: type.name ?? "",
        url: type.url ?? "",
        color: type.color ?? "",
        image: type.image ?? "",
      )).toList(),
      color: item.color,
      pageGroup: item.pageGroup,
      isFavorite: item.isFavorite,
      height: item.height,
      abilities: item.abilities,
      weight: item.weight,
      weaknessModel: item.weakness.map((weak) => WeaknessModel(
        name: weak.name ?? "",
        url: weak.url ?? "",
      )).toList(),
      strenghtsModel: item.strenghts.map((strenght) => StrenghtsModel(
        name: strenght.name ?? "",
        url: strenght.url ?? "",
      )).toList(),
      baseExperience: item.baseExperience,
      statsModel: item.stats.map((stat) => StatsModel(
        name: stat.name ?? "",
        url: stat.url ?? "",
        baseStat: stat.baseStat ?? 0,
      )).toList(),
    )).toList();
  }

  static List<MoveModel> mapToMoveModel(List<MoveCollection> items) {
    return items.map((item) => MoveModel(
      id: item.id ?? 0,
      name: item.name,
      moveTypesModel: item.moves.map((e) => MoveTypeModel(
        move: e.move ?? "",
        type: e.type ?? "",
      )).toList()
    )).toList();
  }

  static List<EvolutionModel> mapToEvolutionModel(List<EvolutionCollection> items) {
    return items.map((item) => EvolutionModel(
      id: item.id ?? 0,
      name: item.name,
      evolutionTypesModel: item.evolutionTypes.map((e) => EvolutionTypeModel(
        name: e.name ?? "",
        url: e.url ?? "",
        type: e.type ?? "",
      )).toList()
    )).toList();
  }
}