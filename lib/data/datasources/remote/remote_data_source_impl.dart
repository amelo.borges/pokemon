import 'package:dio/dio.dart';
import 'package:either_dart/src/either.dart';
import 'package:pokemon/domain/entity/pokemon/pokemon_entity.dart';
import '../../../core/error/failure.dart';
import '../../../core/helpers/helper.dart';
import '../../../core/utils/util.dart';
import 'remote_data_source.dart';

class RemoteDataSourceImpl with Util implements RemoteDataSource {

  final dio = Dio();
  final baseUrl = 'https://pokeapi.co/api/v2/';

  @override
  Future<Either<Failure, dynamic>> fetchPokemon({required int offset,required int limit}) async{
    try{
       final connection = await Helper.hasInternetConnection();
        if(!connection){
          return Left(Failure(message: 'Sem conexão com a internet'));
        }
      final response = await dio.get("${baseUrl}pokemon?offset=$offset&limit=$limit");
      if(response.statusCode == 200){
        return Right(response.data);
      }
      return Left(Failure(message: 'Erro ao buscar pokemon'));
    }catch(e){
      loggerError(message: e);
      return Left(Failure(message: e.toString()));
    }
  }

  @override
  Future<Either<Failure, dynamic>> fetchPokemonUrl({required String url}) async {
    try{
      final connection = await Helper.hasInternetConnection();
      if(!connection){
        return Left(Failure(message: 'Sem conexão com a internet'));
      }
      final response = await dio.get(url);
      if(response.statusCode == 200){
        return Right(response.data);
      }
      return Left(Failure(message: 'Erro ao buscar pokemon'));
    }catch(e){
      loggerError(message: e);
      return Left(Failure(message: e.toString()));
    }
  }


  @override
  Future<Either<Failure, dynamic>> fetchPokemonMove({required PokemonEntity pokemon}) async {
    try{
      final connection = await Helper.hasInternetConnection();
      if(!connection){
        return Left(Failure(message: 'Sem conexão com a internet'));
      }
      final response = await dio.get("${baseUrl}pokemon/${pokemon.name.toLowerCase()}");
      if(response.statusCode == 200){
        List<dynamic> moves = [];
        for(final item in response.data['moves']){
          final moveResponse = await dio.get(item['move']['url']);
          if(moveResponse.statusCode == 200){
            moves.add({
              'move': item['move']['name'],
              'type': moveResponse.data['type']['name']
            });
          }
        }
        return Right(moves);
      }
      return Left(Failure(message: 'Erro ao buscar moves >>>>>'));
    }catch(e){
      loggerError(message: e);
      return Left(Failure(message: e.toString()));
    }
  }


  // Função para obter informações de um Pokémon
  @override
  Future<Either<Failure, dynamic>> fetchPokemonEvolution(String name) async {
    final connection = await Helper.hasInternetConnection();
    if(!connection){
      return Left(Failure(message: 'Sem conexão com a internet'));
    }
    var dio = Dio();
    List<dynamic> evolutions = [];

    // Obtém informações da espécie do Pokémon, que incluem sua cadeia de evolução
    final speciesResponse = await dio.get('${baseUrl}pokemon-species/$name');
    final evolutionChainUrl = speciesResponse.data['evolution_chain']['url'];

    // Obtém a cadeia de evolução
    final evolutionResponse = await dio.get(evolutionChainUrl);
    var currentEvolution = evolutionResponse.data['chain'];

    // Itera pela cadeia de evolução
    do {
      final pokemonName = currentEvolution['species']['name'];

      // Obtém informações detalhadas do Pokémon
      final pokemonResponse = await dio.get('${baseUrl}pokemon/$pokemonName');
      final imageUrl = pokemonResponse.data['sprites']['other']['official-artwork']['front_default'];
      final types = pokemonResponse.data['types']
          .map((typeInfo) => typeInfo['type']['name'])
          .toList();

      evolutions.add({
        'name': pokemonName,
        'url': imageUrl,
        'type': types[0]
      });

      // Move para o próximo estágio da evolução, se houver
      currentEvolution = currentEvolution['evolves_to'].isEmpty ? null : currentEvolution['evolves_to'][0];
    } while (currentEvolution != null);

    return Right(evolutions);
  }



}