import 'package:either_dart/either.dart';
import '../../../core/error/failure.dart';
import '../../../domain/entity/pokemon/pokemon_entity.dart';

abstract class RemoteDataSource {

  Future<Either<Failure, dynamic>> fetchPokemon({required int offset,required int limit});
  Future<Either<Failure, dynamic>> fetchPokemonUrl({required String url});
  Future<Either<Failure, dynamic>> fetchPokemonMove({required PokemonEntity pokemon});
  Future<Either<Failure, dynamic>> fetchPokemonEvolution(String name);
}
