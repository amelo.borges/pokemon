import 'package:either_dart/either.dart';
import 'package:pokemon/core/database/move/move_collection.dart';
import '../../../core/database/evolution/evolution_collection.dart';
import '../../../core/database/pokemon/pokemon_collection.dart';
import '../../../core/error/failure.dart';

abstract class LocalDataSource {
  Future<Either<Failure, List<PokemonCollection>>> getLocalPokemon({required int offset,required int limit});
  Future<Either<Failure, List<PokemonCollection>>> getPokemonFavorites();
  Future<Either<Failure, List<MoveCollection>>> getPokemonMove({required int pokemonId});
  Future<Either<Failure, List<EvolutionCollection>>> getPokemonEvolution({required int pokemonId});
  Future<Either<Failure, bool>> pokemonFavorite({required int pokemonId});
}