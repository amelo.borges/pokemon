import 'package:either_dart/src/either.dart';
import 'package:isar/isar.dart';
import 'package:pokemon/core/database/evolution/evolution_collection.dart';
import 'package:pokemon/core/database/move/move_collection.dart';
import '../../../core/database/isar_schema.dart';
import '../../../core/database/pokemon/pokemon_collection.dart';
import '../../../core/error/failure.dart';
import '../../../core/utils/util.dart';
import 'local_data_source.dart';

class LocalDataSourceImpl with Util implements LocalDataSource {

  Future<Isar> _openDB() async {
    return await IsarSchema.instance.openDB();
  }

  Future<Either<Failure, List<T>>> _getPokemonData<T>(Future<List<T>> Function(Isar) fetchFunction) async {
    try {
      final isar = await _openDB();
      final response = await fetchFunction(isar);
      if (response.isNotEmpty) {
        return Right(response);
      }
      return Left(Failure(message: "No data found in the collection"));
    } catch (e) {
      return Left(Failure(message: e.toString()));
    }
  }


  @override
  Future<Either<Failure, List<PokemonCollection>>> getLocalPokemon({required int offset, required int limit}) async {
    return _getPokemonData((isar) => isar.pokemonCollections.where().filter().idGreaterThan(offset).limit(limit).findAll());
  }

  @override
  Future<Either<Failure, List<PokemonCollection>>> getPokemonFavorites() async {
    return _getPokemonData((isar) => isar.pokemonCollections.where().filter().isFavoriteEqualTo(true).findAll());
  }

  @override
  Future<Either<Failure, List<MoveCollection>>> getPokemonMove({required int pokemonId}) async {
    return _getPokemonData((isar) => isar.moveCollections.where().filter().idEqualTo(pokemonId).findAll());

  }

  @override
  Future<Either<Failure, List<EvolutionCollection>>> getPokemonEvolution({required int pokemonId}) async {
    return _getPokemonData((isar) => isar.evolutionCollections.where().filter().idEqualTo(pokemonId).findAll());
  }

  @override
  Future<Either<Failure, bool>> pokemonFavorite({required int pokemonId}) async  {
    final response = await _getPokemonData((isar) => isar.pokemonCollections.where().filter().idEqualTo(pokemonId).findAll());
    if(response.isRight) {
         final pokemon = response.right;
         final isar = await _openDB();
         final pokemonCollection = pokemon.first;
         pokemonCollection.isFavorite = !pokemonCollection.isFavorite;
         await isar.writeTxn(() async {
           await isar.pokemonCollections.put(pokemonCollection);
         });
         return Right(pokemonCollection.isFavorite);
      }
      return Left(Failure(message: "No data found in the collection"));
    }
}