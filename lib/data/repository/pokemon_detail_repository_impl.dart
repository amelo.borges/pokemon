import 'package:either_dart/src/either.dart';
import 'package:pokemon/domain/entity/evolution/evolution_entity.dart';
import 'package:pokemon/domain/entity/pokemon/pokemon_entity.dart';
import '../../core/database/save_data.dart';
import '../../core/error/failure.dart';
import '../../core/utils/util.dart';
import '../../domain/entity/move/move_entity.dart';
import '../../domain/repository/pokemon__detail_repository.dart';
import '../datasources/local/local_data_source.dart';
import '../datasources/remote/remote_data_source.dart';
import '../mapping/mapping.dart';

class PokemonDetailRepositoryImpl with Util implements PokemonDetailRepository{
  final RemoteDataSource remote;
  final LocalDataSource local;
  PokemonDetailRepositoryImpl({required this.remote, required this.local});


  @override
  Future<Either<Failure, List<MoveEntity>>> fetchPokemonMove({required PokemonEntity pokemon}) async {

      int attempts = 0;
      while (attempts < 2) {

        // Tenta obter os dados locais
        final localData = await local.getPokemonMove(pokemonId: pokemon.id);

        // Se os dados locais existem e estão corretos
        if(localData.isRight){

          // Mapeia os dados para o modelo de Pokemon e retorna
          final map = Mapping.mapToMoveModel(localData.right);
          return Right(map);

        } else {
          // Se os dados locais não existem ou estão incorretos, busca os dados da API
          await _fetchMove(pokemon);
        }
        // Incrementa a contagem de tentativas
        attempts++;
      }
      return Left(Failure(message: 'Erro ao buscar move'));
  }

  Future _fetchMove(PokemonEntity pokemon) async {
    final response = await remote.fetchPokemonMove(pokemon: pokemon);
    if (response.isRight) {
        await SaveData.move(items : response.right,pokemon: pokemon);
    }
  }

  @override
  Future<Either<Failure, List<EvolutionEntity>>> fetchPokemonEvolution({required PokemonEntity pokemon}) async {
    int attempts = 0;
    while (attempts < 2) {

      loggerWarning(message: attempts);

      // Tenta obter os dados locais
      final localData = await local.getPokemonEvolution(pokemonId: pokemon.id);

      // Se os dados locais existem e estão corretos
      if(localData.isRight){

        // Mapeia os dados para o modelo de Pokemon e retorna
        final map = Mapping.mapToEvolutionModel(localData.right);
        return Right(map);

      } else {
        // Se os dados locais não existem ou estão incorretos, busca os dados da API
        await _fetchEvolution(pokemon);
      }
      // Incrementa a contagem de tentativas
      attempts++;
    }
    return Left(Failure(message: 'Erro ao buscar evolução'));
  }


  Future _fetchEvolution(PokemonEntity pokemon) async {
    final response =  await remote.fetchPokemonEvolution(pokemon.name.toLowerCase());
    if(response.isRight){
      loggerWarning(message: response.right);
      await SaveData.evolution(items: response.right,pokemon: pokemon);
    }
  }




  @override
  Future<Either<Failure, bool>> pokemonFavorite({required int pokemonId}) {
    return local.pokemonFavorite(pokemonId: pokemonId);
  }



}