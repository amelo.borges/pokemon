import 'package:either_dart/src/either.dart';
import 'package:pokemon/core/error/failure.dart';
import 'package:pokemon/domain/entity/pokemon/pokemon_entity.dart';
import '../../domain/repository/favorites_repository.dart';
import '../datasources/local/local_data_source.dart';
import '../mapping/mapping.dart';

class FavoritesRepositoryImpl implements FavoritesRepository {

  final LocalDataSource local;
  FavoritesRepositoryImpl({required this.local});


  @override
  Future<Either<Failure, List<PokemonEntity>>> fetchPokemonFavorites() async {
    final response = await local.getPokemonFavorites();
    if(response.isRight){
      final map = Mapping.mapToPokemonModel(response.right);
      return Right(map);
    }
    return Left(Failure(message: 'Erro ao buscar pokemon'));
  }

  @override
  Future<Either<Failure, bool>> pokemonFavorite({required int pokemonId}) {
    return local.pokemonFavorite(pokemonId: pokemonId);
  }

}
