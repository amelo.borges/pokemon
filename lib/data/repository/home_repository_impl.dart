import 'package:either_dart/src/either.dart';
import 'package:pokemon/core/database/save_data.dart';
import '../../core/error/failure.dart';
import '../../core/utils/util.dart';
import '../../domain/entity/pokemon/pokemon_entity.dart';
import '../../domain/repository/home_repository.dart';
import '../datasources/local/local_data_source.dart';
import '../datasources/remote/remote_data_source.dart';
import '../mapping/mapping.dart';

class HomeRepositoryImpl with Util implements HomeRepository {
  final RemoteDataSource remote;
  final LocalDataSource local;

  HomeRepositoryImpl({required this.remote, required this.local});

  @override
  Future<Either<Failure, List<PokemonEntity>>> fetchPokemon({required int offset,required int limit}) async {
    int attempts = 0;
    while (attempts < 2) {
      final localData = await local.getLocalPokemon(offset: offset, limit: limit);
      if(localData.isRight){
        return Right(Mapping.mapToPokemonModel(localData.right));
      } else {
        await _fetchAndSavePokemon(offset, limit);
        attempts++;
      }
    }
    return Left(Failure(message: 'Erro ao buscar pokemon'));
  }

  Future _fetchAndSavePokemon(int offset, int limit) async {
    final response = await remote.fetchPokemon(offset: offset, limit: limit);
    if (response.isRight) {
      await _fetchAndSavePokemonDetails(response.right['results'], offset);
    }
  }

  Future _fetchAndSavePokemonDetails(List<dynamic> results, int offset) async {
    for (final item in results) {
      final response = await remote.fetchPokemonUrl(url: item['url']);
      if (response.isRight) {
        final pokemon = response.right;
        final damagesAndStrengths = await _fetchDamagesAndStrengths(pokemon['types']);
        await SaveData.pokemon(
            item: pokemon,
            damages: damagesAndStrengths[0],
            strenghts: damagesAndStrengths[1],
            offset: offset);
      }
    }
  }

  Future<List<dynamic>> _fetchDamagesAndStrengths(List<dynamic> types) async {
    List<dynamic> damages = [];
    List<dynamic> strenghts = [];
    for(final item in types){
      final type = await remote.fetchPokemonUrl(url: item['type']['url']);
      if(type.isRight){
        damages.add(type.right['damage_relations']['double_damage_from']);
        strenghts.add(type.right['damage_relations']['half_damage_to']);
      }
    }
    return [damages[0], strenghts[0]];
  }
}