import 'package:get/get.dart';
import 'package:isar/isar.dart';
import 'package:pokemon/core/database/move/move_collection.dart';
import 'package:pokemon/core/database/pokemon/pokemon_collection.dart';
import 'package:pokemon/core/database/pokemon/type_embedded.dart';
import 'package:pokemon/domain/entity/pokemon/pokemon_entity.dart';

import '../utils/util.dart';
import 'evolution/evolution_collection.dart';
import 'evolution/evolution_type_embedded.dart';
import 'isar_schema.dart';
import 'move/move_embedded.dart';
import 'pokemon/stats_embedded.dart';
import 'pokemon/strenghts_embedded.dart';
import 'pokemon/weakness_embedded.dart';

class SaveData {

  static Future pokemon({required dynamic item, required dynamic damages,required dynamic strenghts, required int offset}) async{
    try {
      final isar = await _openDB();
      await isar.writeTxn(() async {

       final obj = PokemonCollection(
            id: item['id'],
            name: Util.capitalize(item['name']),
            image: item['sprites']['other']['official-artwork']['front_default'],
            isFavorite: false,
            pageGroup: offset,
            height: item['height'].toString(),
            baseExperience: item['base_experience'],
            abilities: (item['abilities'] as List<dynamic>).map((abilityItem) {
              var ability = abilityItem['ability'];
              var abilityName = ability['name'];
              return Util.capitalize(abilityName);
            }).toList(),
            weight: item['weight'].toString(),

            types: (item['types'] as List<dynamic>).map((typeItem) {
              var type = typeItem['type'];
              var typeName = type['name'];
              return TypeEmbedded(
                name: Util.capitalize(typeName),
                url: type['url'],
                color: typeName,
                image: Util.getImage(typeName),
              );
            }).toList(),

            color: Util.capitalize(item['types'][0]['type']['name']),

            weakness: (damages as List<dynamic>).map((weaknessItem) {
              var weaknessName = weaknessItem['name'];
              return WeaknessEmbedded(
                name: Util.capitalize(weaknessName),
                url: weaknessItem['url'],
              );
            }).toList(),


            strenghts: (strenghts as List<dynamic>).map((strenghtItem) {
              var strenghtName = strenghtItem['name'];
              return StrenghtsEmbedded(
                name: Util.capitalize(strenghtName),
                url: strenghtItem['url'],
              );
            }).toList(),

            stats: (item['stats'] as List<dynamic>).map((statsItem) {
              var statsName = statsItem['stat']['name'];
              return StatsEmbedded(
                name: Util.capitalize(statsName),
                url: statsItem['stat']['url'],
                baseStat: statsItem['base_stat'],
              );
            }).toList(),
       );

       await isar.pokemonCollections.put(obj);

      });
    } catch (e) {
      print("Error: ${e.toString()}");
    }
  }


  static Future move({required List<dynamic> items,required PokemonEntity pokemon}) async{
    try{
      final isar = await _openDB();
      await isar.writeTxn(() async {
       final obj = MoveCollection(
          id: pokemon.id,
          name: pokemon.name,
          moves: items.map((move) {
            return MoveEmbedded(
              move: move['move'],
              type: move['type']
            );
          }).toList()
        );
       await isar.moveCollections.put(obj);
      });
    }catch(e){
      print("Error: ${e.toString()}");
    }
  }


  static Future evolution({required List<dynamic> items,required PokemonEntity pokemon}) async{
    try{
      final isar = await _openDB();
      await isar.writeTxn(() async {
       final obj = EvolutionCollection(
          id: pokemon.id,
          name: pokemon.name,
          evolutionTypes: items.map((evolution) {
            List<String> types = [];
            // for(final item in evolution['types']){
            //   types.add(item.toString());
            // }
            return EvolutionTypeEmbedded(
              name: evolution['name'],
              url: evolution['url'],
              type: evolution["type"],
            );
          }).toList()
        );
       await isar.evolutionCollections.put(obj);
      });
    }catch(e){
      print("Error: ${e.toString()}");
    }
  }


  static Future<Isar> _openDB() async {
    return await IsarSchema.instance.openDB();
  }
}
