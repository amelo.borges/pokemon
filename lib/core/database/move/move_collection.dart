import 'package:isar/isar.dart';
import 'move_embedded.dart';

part 'move_collection.g.dart';

@collection
class MoveCollection {
  Id? id;
  String name;
  List<MoveEmbedded> moves;

  MoveCollection({
    required this.id,
    required this.name,
    required this.moves,
  });
}