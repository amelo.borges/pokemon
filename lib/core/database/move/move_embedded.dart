import 'package:isar/isar.dart';

part 'move_embedded.g.dart';

@embedded
class MoveEmbedded{
  String? move;
  String? type;
  MoveEmbedded({
    this.move,
    this.type,
  });
}