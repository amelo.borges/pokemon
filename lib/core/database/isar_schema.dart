import 'package:isar/isar.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pokemon/core/database/pokemon/pokemon_collection.dart';

import 'evolution/evolution_collection.dart';
import 'move/move_collection.dart';

class IsarSchema {
  IsarSchema._internal();
  static final IsarSchema _instance = IsarSchema._internal();
  static IsarSchema get instance => _instance;

  Future<Isar> openDB() async {
    List<CollectionSchema<dynamic>> schemas = [
      PokemonCollectionSchema,
      MoveCollectionSchema,
      EvolutionCollectionSchema,
    ];
    final appDocumentDirectory = await getApplicationDocumentsDirectory();
    if (Isar.instanceNames.isEmpty) {
      return await Isar.open(schemas, inspector: true, directory: appDocumentDirectory.path);
    }
    return Future.value(Isar.getInstance());
  }

  Future<Isar> getSchema(List<CollectionSchema<dynamic>> schema) async {
    final appDocumentDirectory = await getApplicationDocumentsDirectory();

    return await Isar.open(schema, directory: appDocumentDirectory.path, inspector: true);
  }
}