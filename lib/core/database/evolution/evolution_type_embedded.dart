import 'package:isar/isar.dart';

part 'evolution_type_embedded.g.dart';

@embedded
class EvolutionTypeEmbedded {
  String? name;
  String? url;
  String? type;


  EvolutionTypeEmbedded({
    this.name,
    this.url,
    this.type,
  });
}