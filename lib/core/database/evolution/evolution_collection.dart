import 'package:isar/isar.dart';
import 'evolution_type_embedded.dart';

part 'evolution_collection.g.dart';

@collection
class EvolutionCollection {
  Id? id;
  String name;
  List<EvolutionTypeEmbedded> evolutionTypes;

  EvolutionCollection({
    required this.id,
    required this.name,
    required this.evolutionTypes,
  });
}
