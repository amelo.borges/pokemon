import 'package:isar/isar.dart';

part 'type_embedded.g.dart';

@embedded
class TypeEmbedded{
  String? name;
  String? url;
  String? color;
  String? image;

  TypeEmbedded({
    this.name,
    this.url,
    this.color,
    this.image,
});
}