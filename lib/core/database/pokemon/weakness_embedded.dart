import 'package:isar/isar.dart';

part 'weakness_embedded.g.dart';

@embedded
class WeaknessEmbedded {
  String? name;
  String? url;

  WeaknessEmbedded({
    this.name,
    this.url
  });
}