import 'package:isar/isar.dart';
part 'stats_embedded.g.dart';

@embedded
class StatsEmbedded {
  int? baseStat;
  String? name;
  String? url;

  StatsEmbedded({
    this.baseStat,
    this.name,
    this.url,
  });
}