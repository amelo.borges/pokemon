import 'package:isar/isar.dart';

part 'strenghts_embedded.g.dart';

@embedded
class StrenghtsEmbedded{
  String? name;
  String? url;

  StrenghtsEmbedded({
    this.name,
    this.url
  });
}