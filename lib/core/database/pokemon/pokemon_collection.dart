import 'package:isar/isar.dart';
import 'package:pokemon/core/database/pokemon/type_embedded.dart';
import 'stats_embedded.dart';
import 'strenghts_embedded.dart';
import 'weakness_embedded.dart';

part 'pokemon_collection.g.dart';

@Collection()
class PokemonCollection {
  Id id;
  String name;
  String image;
  String color;
  bool isFavorite;
  int pageGroup;
  List<TypeEmbedded> types;
  String height;
  List<String> abilities;
  String weight;
  List<WeaknessEmbedded> weakness;
  List<StrenghtsEmbedded> strenghts;
  List<StatsEmbedded> stats;
  int baseExperience;

  PokemonCollection({
    required this.id,
    required this.name,
    required this.image,
    required this.types,
    required this.color,
    required this.pageGroup,
    required this.isFavorite,
    required this.height,
    required this.abilities,
    required this.weight,
    required this.weakness,
    required this.strenghts,
    required this.stats,
    required this.baseExperience,
  });
}