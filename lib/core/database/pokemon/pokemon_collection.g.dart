// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pokemon_collection.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetPokemonCollectionCollection on Isar {
  IsarCollection<PokemonCollection> get pokemonCollections => this.collection();
}

const PokemonCollectionSchema = CollectionSchema(
  name: r'PokemonCollection',
  id: 3791656485791643456,
  properties: {
    r'abilities': PropertySchema(
      id: 0,
      name: r'abilities',
      type: IsarType.stringList,
    ),
    r'baseExperience': PropertySchema(
      id: 1,
      name: r'baseExperience',
      type: IsarType.long,
    ),
    r'color': PropertySchema(
      id: 2,
      name: r'color',
      type: IsarType.string,
    ),
    r'height': PropertySchema(
      id: 3,
      name: r'height',
      type: IsarType.string,
    ),
    r'image': PropertySchema(
      id: 4,
      name: r'image',
      type: IsarType.string,
    ),
    r'isFavorite': PropertySchema(
      id: 5,
      name: r'isFavorite',
      type: IsarType.bool,
    ),
    r'name': PropertySchema(
      id: 6,
      name: r'name',
      type: IsarType.string,
    ),
    r'pageGroup': PropertySchema(
      id: 7,
      name: r'pageGroup',
      type: IsarType.long,
    ),
    r'stats': PropertySchema(
      id: 8,
      name: r'stats',
      type: IsarType.objectList,
      target: r'StatsEmbedded',
    ),
    r'strenghts': PropertySchema(
      id: 9,
      name: r'strenghts',
      type: IsarType.objectList,
      target: r'StrenghtsEmbedded',
    ),
    r'types': PropertySchema(
      id: 10,
      name: r'types',
      type: IsarType.objectList,
      target: r'TypeEmbedded',
    ),
    r'weakness': PropertySchema(
      id: 11,
      name: r'weakness',
      type: IsarType.objectList,
      target: r'WeaknessEmbedded',
    ),
    r'weight': PropertySchema(
      id: 12,
      name: r'weight',
      type: IsarType.string,
    )
  },
  estimateSize: _pokemonCollectionEstimateSize,
  serialize: _pokemonCollectionSerialize,
  deserialize: _pokemonCollectionDeserialize,
  deserializeProp: _pokemonCollectionDeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {
    r'TypeEmbedded': TypeEmbeddedSchema,
    r'WeaknessEmbedded': WeaknessEmbeddedSchema,
    r'StrenghtsEmbedded': StrenghtsEmbeddedSchema,
    r'StatsEmbedded': StatsEmbeddedSchema
  },
  getId: _pokemonCollectionGetId,
  getLinks: _pokemonCollectionGetLinks,
  attach: _pokemonCollectionAttach,
  version: '3.1.0+1',
);

int _pokemonCollectionEstimateSize(
  PokemonCollection object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  bytesCount += 3 + object.abilities.length * 3;
  {
    for (var i = 0; i < object.abilities.length; i++) {
      final value = object.abilities[i];
      bytesCount += value.length * 3;
    }
  }
  bytesCount += 3 + object.color.length * 3;
  bytesCount += 3 + object.height.length * 3;
  bytesCount += 3 + object.image.length * 3;
  bytesCount += 3 + object.name.length * 3;
  bytesCount += 3 + object.stats.length * 3;
  {
    final offsets = allOffsets[StatsEmbedded]!;
    for (var i = 0; i < object.stats.length; i++) {
      final value = object.stats[i];
      bytesCount +=
          StatsEmbeddedSchema.estimateSize(value, offsets, allOffsets);
    }
  }
  bytesCount += 3 + object.strenghts.length * 3;
  {
    final offsets = allOffsets[StrenghtsEmbedded]!;
    for (var i = 0; i < object.strenghts.length; i++) {
      final value = object.strenghts[i];
      bytesCount +=
          StrenghtsEmbeddedSchema.estimateSize(value, offsets, allOffsets);
    }
  }
  bytesCount += 3 + object.types.length * 3;
  {
    final offsets = allOffsets[TypeEmbedded]!;
    for (var i = 0; i < object.types.length; i++) {
      final value = object.types[i];
      bytesCount += TypeEmbeddedSchema.estimateSize(value, offsets, allOffsets);
    }
  }
  bytesCount += 3 + object.weakness.length * 3;
  {
    final offsets = allOffsets[WeaknessEmbedded]!;
    for (var i = 0; i < object.weakness.length; i++) {
      final value = object.weakness[i];
      bytesCount +=
          WeaknessEmbeddedSchema.estimateSize(value, offsets, allOffsets);
    }
  }
  bytesCount += 3 + object.weight.length * 3;
  return bytesCount;
}

void _pokemonCollectionSerialize(
  PokemonCollection object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeStringList(offsets[0], object.abilities);
  writer.writeLong(offsets[1], object.baseExperience);
  writer.writeString(offsets[2], object.color);
  writer.writeString(offsets[3], object.height);
  writer.writeString(offsets[4], object.image);
  writer.writeBool(offsets[5], object.isFavorite);
  writer.writeString(offsets[6], object.name);
  writer.writeLong(offsets[7], object.pageGroup);
  writer.writeObjectList<StatsEmbedded>(
    offsets[8],
    allOffsets,
    StatsEmbeddedSchema.serialize,
    object.stats,
  );
  writer.writeObjectList<StrenghtsEmbedded>(
    offsets[9],
    allOffsets,
    StrenghtsEmbeddedSchema.serialize,
    object.strenghts,
  );
  writer.writeObjectList<TypeEmbedded>(
    offsets[10],
    allOffsets,
    TypeEmbeddedSchema.serialize,
    object.types,
  );
  writer.writeObjectList<WeaknessEmbedded>(
    offsets[11],
    allOffsets,
    WeaknessEmbeddedSchema.serialize,
    object.weakness,
  );
  writer.writeString(offsets[12], object.weight);
}

PokemonCollection _pokemonCollectionDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = PokemonCollection(
    abilities: reader.readStringList(offsets[0]) ?? [],
    baseExperience: reader.readLong(offsets[1]),
    color: reader.readString(offsets[2]),
    height: reader.readString(offsets[3]),
    id: id,
    image: reader.readString(offsets[4]),
    isFavorite: reader.readBool(offsets[5]),
    name: reader.readString(offsets[6]),
    pageGroup: reader.readLong(offsets[7]),
    stats: reader.readObjectList<StatsEmbedded>(
          offsets[8],
          StatsEmbeddedSchema.deserialize,
          allOffsets,
          StatsEmbedded(),
        ) ??
        [],
    strenghts: reader.readObjectList<StrenghtsEmbedded>(
          offsets[9],
          StrenghtsEmbeddedSchema.deserialize,
          allOffsets,
          StrenghtsEmbedded(),
        ) ??
        [],
    types: reader.readObjectList<TypeEmbedded>(
          offsets[10],
          TypeEmbeddedSchema.deserialize,
          allOffsets,
          TypeEmbedded(),
        ) ??
        [],
    weakness: reader.readObjectList<WeaknessEmbedded>(
          offsets[11],
          WeaknessEmbeddedSchema.deserialize,
          allOffsets,
          WeaknessEmbedded(),
        ) ??
        [],
    weight: reader.readString(offsets[12]),
  );
  return object;
}

P _pokemonCollectionDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringList(offset) ?? []) as P;
    case 1:
      return (reader.readLong(offset)) as P;
    case 2:
      return (reader.readString(offset)) as P;
    case 3:
      return (reader.readString(offset)) as P;
    case 4:
      return (reader.readString(offset)) as P;
    case 5:
      return (reader.readBool(offset)) as P;
    case 6:
      return (reader.readString(offset)) as P;
    case 7:
      return (reader.readLong(offset)) as P;
    case 8:
      return (reader.readObjectList<StatsEmbedded>(
            offset,
            StatsEmbeddedSchema.deserialize,
            allOffsets,
            StatsEmbedded(),
          ) ??
          []) as P;
    case 9:
      return (reader.readObjectList<StrenghtsEmbedded>(
            offset,
            StrenghtsEmbeddedSchema.deserialize,
            allOffsets,
            StrenghtsEmbedded(),
          ) ??
          []) as P;
    case 10:
      return (reader.readObjectList<TypeEmbedded>(
            offset,
            TypeEmbeddedSchema.deserialize,
            allOffsets,
            TypeEmbedded(),
          ) ??
          []) as P;
    case 11:
      return (reader.readObjectList<WeaknessEmbedded>(
            offset,
            WeaknessEmbeddedSchema.deserialize,
            allOffsets,
            WeaknessEmbedded(),
          ) ??
          []) as P;
    case 12:
      return (reader.readString(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _pokemonCollectionGetId(PokemonCollection object) {
  return object.id;
}

List<IsarLinkBase<dynamic>> _pokemonCollectionGetLinks(
    PokemonCollection object) {
  return [];
}

void _pokemonCollectionAttach(
    IsarCollection<dynamic> col, Id id, PokemonCollection object) {
  object.id = id;
}

extension PokemonCollectionQueryWhereSort
    on QueryBuilder<PokemonCollection, PokemonCollection, QWhere> {
  QueryBuilder<PokemonCollection, PokemonCollection, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension PokemonCollectionQueryWhere
    on QueryBuilder<PokemonCollection, PokemonCollection, QWhereClause> {
  QueryBuilder<PokemonCollection, PokemonCollection, QAfterWhereClause>
      idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterWhereClause>
      idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterWhereClause>
      idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterWhereClause>
      idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterWhereClause>
      idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension PokemonCollectionQueryFilter
    on QueryBuilder<PokemonCollection, PokemonCollection, QFilterCondition> {
  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      abilitiesElementEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'abilities',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      abilitiesElementGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'abilities',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      abilitiesElementLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'abilities',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      abilitiesElementBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'abilities',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      abilitiesElementStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'abilities',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      abilitiesElementEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'abilities',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      abilitiesElementContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'abilities',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      abilitiesElementMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'abilities',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      abilitiesElementIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'abilities',
        value: '',
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      abilitiesElementIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'abilities',
        value: '',
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      abilitiesLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'abilities',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      abilitiesIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'abilities',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      abilitiesIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'abilities',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      abilitiesLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'abilities',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      abilitiesLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'abilities',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      abilitiesLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'abilities',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      baseExperienceEqualTo(int value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'baseExperience',
        value: value,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      baseExperienceGreaterThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'baseExperience',
        value: value,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      baseExperienceLessThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'baseExperience',
        value: value,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      baseExperienceBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'baseExperience',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      colorEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      colorGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      colorLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      colorBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'color',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      colorStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      colorEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      colorContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      colorMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'color',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      colorIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'color',
        value: '',
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      colorIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'color',
        value: '',
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      heightEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'height',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      heightGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'height',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      heightLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'height',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      heightBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'height',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      heightStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'height',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      heightEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'height',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      heightContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'height',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      heightMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'height',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      heightIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'height',
        value: '',
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      heightIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'height',
        value: '',
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      idEqualTo(Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      idGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      idLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      idBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      imageEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'image',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      imageGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'image',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      imageLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'image',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      imageBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'image',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      imageStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'image',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      imageEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'image',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      imageContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'image',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      imageMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'image',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      imageIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'image',
        value: '',
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      imageIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'image',
        value: '',
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      isFavoriteEqualTo(bool value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'isFavorite',
        value: value,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      nameEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      nameGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      nameLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      nameBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'name',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      nameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      nameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      nameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      nameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'name',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      nameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      nameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      pageGroupEqualTo(int value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'pageGroup',
        value: value,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      pageGroupGreaterThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'pageGroup',
        value: value,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      pageGroupLessThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'pageGroup',
        value: value,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      pageGroupBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'pageGroup',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      statsLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'stats',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      statsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'stats',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      statsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'stats',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      statsLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'stats',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      statsLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'stats',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      statsLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'stats',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      strenghtsLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'strenghts',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      strenghtsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'strenghts',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      strenghtsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'strenghts',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      strenghtsLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'strenghts',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      strenghtsLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'strenghts',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      strenghtsLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'strenghts',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      typesLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'types',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      typesIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'types',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      typesIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'types',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      typesLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'types',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      typesLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'types',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      typesLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'types',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      weaknessLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'weakness',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      weaknessIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'weakness',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      weaknessIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'weakness',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      weaknessLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'weakness',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      weaknessLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'weakness',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      weaknessLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'weakness',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      weightEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'weight',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      weightGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'weight',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      weightLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'weight',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      weightBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'weight',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      weightStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'weight',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      weightEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'weight',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      weightContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'weight',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      weightMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'weight',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      weightIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'weight',
        value: '',
      ));
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      weightIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'weight',
        value: '',
      ));
    });
  }
}

extension PokemonCollectionQueryObject
    on QueryBuilder<PokemonCollection, PokemonCollection, QFilterCondition> {
  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      statsElement(FilterQuery<StatsEmbedded> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'stats');
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      strenghtsElement(FilterQuery<StrenghtsEmbedded> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'strenghts');
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      typesElement(FilterQuery<TypeEmbedded> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'types');
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterFilterCondition>
      weaknessElement(FilterQuery<WeaknessEmbedded> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'weakness');
    });
  }
}

extension PokemonCollectionQueryLinks
    on QueryBuilder<PokemonCollection, PokemonCollection, QFilterCondition> {}

extension PokemonCollectionQuerySortBy
    on QueryBuilder<PokemonCollection, PokemonCollection, QSortBy> {
  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      sortByBaseExperience() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'baseExperience', Sort.asc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      sortByBaseExperienceDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'baseExperience', Sort.desc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      sortByColor() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.asc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      sortByColorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.desc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      sortByHeight() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'height', Sort.asc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      sortByHeightDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'height', Sort.desc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      sortByImage() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'image', Sort.asc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      sortByImageDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'image', Sort.desc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      sortByIsFavorite() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isFavorite', Sort.asc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      sortByIsFavoriteDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isFavorite', Sort.desc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      sortByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      sortByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      sortByPageGroup() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'pageGroup', Sort.asc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      sortByPageGroupDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'pageGroup', Sort.desc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      sortByWeight() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'weight', Sort.asc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      sortByWeightDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'weight', Sort.desc);
    });
  }
}

extension PokemonCollectionQuerySortThenBy
    on QueryBuilder<PokemonCollection, PokemonCollection, QSortThenBy> {
  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      thenByBaseExperience() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'baseExperience', Sort.asc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      thenByBaseExperienceDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'baseExperience', Sort.desc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      thenByColor() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.asc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      thenByColorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.desc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      thenByHeight() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'height', Sort.asc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      thenByHeightDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'height', Sort.desc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      thenByImage() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'image', Sort.asc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      thenByImageDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'image', Sort.desc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      thenByIsFavorite() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isFavorite', Sort.asc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      thenByIsFavoriteDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isFavorite', Sort.desc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      thenByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      thenByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      thenByPageGroup() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'pageGroup', Sort.asc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      thenByPageGroupDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'pageGroup', Sort.desc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      thenByWeight() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'weight', Sort.asc);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QAfterSortBy>
      thenByWeightDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'weight', Sort.desc);
    });
  }
}

extension PokemonCollectionQueryWhereDistinct
    on QueryBuilder<PokemonCollection, PokemonCollection, QDistinct> {
  QueryBuilder<PokemonCollection, PokemonCollection, QDistinct>
      distinctByAbilities() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'abilities');
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QDistinct>
      distinctByBaseExperience() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'baseExperience');
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QDistinct> distinctByColor(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'color', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QDistinct>
      distinctByHeight({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'height', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QDistinct> distinctByImage(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'image', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QDistinct>
      distinctByIsFavorite() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'isFavorite');
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QDistinct> distinctByName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'name', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QDistinct>
      distinctByPageGroup() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'pageGroup');
    });
  }

  QueryBuilder<PokemonCollection, PokemonCollection, QDistinct>
      distinctByWeight({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'weight', caseSensitive: caseSensitive);
    });
  }
}

extension PokemonCollectionQueryProperty
    on QueryBuilder<PokemonCollection, PokemonCollection, QQueryProperty> {
  QueryBuilder<PokemonCollection, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<PokemonCollection, List<String>, QQueryOperations>
      abilitiesProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'abilities');
    });
  }

  QueryBuilder<PokemonCollection, int, QQueryOperations>
      baseExperienceProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'baseExperience');
    });
  }

  QueryBuilder<PokemonCollection, String, QQueryOperations> colorProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'color');
    });
  }

  QueryBuilder<PokemonCollection, String, QQueryOperations> heightProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'height');
    });
  }

  QueryBuilder<PokemonCollection, String, QQueryOperations> imageProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'image');
    });
  }

  QueryBuilder<PokemonCollection, bool, QQueryOperations> isFavoriteProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'isFavorite');
    });
  }

  QueryBuilder<PokemonCollection, String, QQueryOperations> nameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'name');
    });
  }

  QueryBuilder<PokemonCollection, int, QQueryOperations> pageGroupProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'pageGroup');
    });
  }

  QueryBuilder<PokemonCollection, List<StatsEmbedded>, QQueryOperations>
      statsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'stats');
    });
  }

  QueryBuilder<PokemonCollection, List<StrenghtsEmbedded>, QQueryOperations>
      strenghtsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'strenghts');
    });
  }

  QueryBuilder<PokemonCollection, List<TypeEmbedded>, QQueryOperations>
      typesProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'types');
    });
  }

  QueryBuilder<PokemonCollection, List<WeaknessEmbedded>, QQueryOperations>
      weaknessProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'weakness');
    });
  }

  QueryBuilder<PokemonCollection, String, QQueryOperations> weightProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'weight');
    });
  }
}
