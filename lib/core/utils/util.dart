import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:logger/logger.dart';
import 'package:pokemon/core/values/colors.dart';

import '../values/images.dart';

mixin Util {
  FToast fToast = FToast();

  final _logger = Logger(
    printer: PrettyPrinter(
        methodCount: 2,
        errorMethodCount: 8,
        lineLength: 120,
        colors: true,
        printEmojis: true,
        printTime: false
    ),
  );

  loggerVerbose({required dynamic message}) {_logger.v(message);}
  loggerDebug({required dynamic message}) {_logger.d(message);}
  loggerInfo({required dynamic message}) {_logger.i(message);}
  loggerWarning({required dynamic message}) {_logger.w(message);}
  loggerError({required dynamic message}) {_logger.e(message);}
  loggerWtf({required dynamic message}) {_logger.wtf(message);}



  static double getTextWidth(String text) {
    final textPainter = TextPainter(
      text: TextSpan(text: text),
      maxLines: 1,
      textDirection: TextDirection.ltr,
    )..layout(minWidth: 0, maxWidth: double.infinity);
    return textPainter.size.width;
  }


  static getColor(typeItem) {
    switch (typeItem) {
      case 'normal':
        return TypeColors.normal;
      case 'fire':
        return TypeColors.fire;
      case 'water':
        return TypeColors.water;
      case 'electric':
        return TypeColors.electric;
      case 'grass':
        return TypeColors.grass;
      case 'ice':
        return TypeColors.ice;
      case 'fighting':
        return TypeColors.fighting;
      case 'poison':
        return TypeColors.poison;
      case 'ground':
        return TypeColors.ground;
      case 'flying':
        return TypeColors.flying;
      case 'psychic':
        return TypeColors.psychic;
      case 'bug':
        return TypeColors.bug;
      case 'rock':
        return TypeColors.rock;
      case 'ghost':
        return TypeColors.ghost;
      case 'dragon':
        return TypeColors.dragon;
      case 'dark':
        return TypeColors.dark;
      case 'steel':
        return TypeColors.steel;
      case 'fairy':
        return TypeColors.fairy;
      default:
        return Colors.black;
    }
  }



  static getCardColor({required String typeItem, bool isDark = false}) {
    switch (typeItem.toLowerCase()) {
      case 'normal':
        return isDark ? CardColorsDark.normal : CardColors.normal;
      case 'fire':
        return isDark ? CardColorsDark.fire : CardColors.fire;
      case 'water':
        return isDark ? CardColorsDark.water : CardColors.water;
      case 'electric':
        return isDark ? CardColorsDark.electric : CardColors.electric;
      case 'grass':
        return isDark ? CardColorsDark.grass : CardColors.grass;
      case 'ice':
        return isDark ? CardColorsDark.ice : CardColors.ice;
      case 'fighting':
        return isDark ? CardColorsDark.fighting : CardColors.fighting;
      case 'poison':
        return isDark ? CardColorsDark.poison : CardColors.poison;
      case 'ground':
        return isDark ? CardColorsDark.ground : CardColors.ground;
      case 'flying':
        return isDark ? CardColorsDark.flying : CardColors.flying;
      case 'psychic':
        return isDark ? CardColorsDark.psychic : CardColors.psychic;
      case 'bug':
        return isDark ? CardColorsDark.bug : CardColors.bug;
      case 'rock':
        return isDark ? CardColorsDark.rock : CardColors.rock;
      case 'ghost':
        return isDark ? CardColorsDark.ghost : CardColors.ghost;
      case 'dragon':
        return isDark ? CardColorsDark.dragon : CardColors.dragon;
      case 'dark':
        return isDark ? CardColorsDark.dark : CardColors.dark;
      case 'steel':
        return isDark ? CardColorsDark.steel : CardColors.steel;
      case 'fairy':
        return isDark ? CardColorsDark.fairy : CardColors.fairy;
      default:
        return isDark ? CardColorsDark.normal : CardColors.normal;
    }
  }

  static getImage(name) {
    switch(name) {
      case 'normal':
        return ImageType.normal;
      case 'fire':
        return ImageType.fire;
      case 'water':
        return ImageType.water;
      case 'electric':
        return ImageType.electric;
      case 'grass':
        return ImageType.grass;
      case 'ice':
        return ImageType.ice;
      case 'fighting':
        return ImageType.fighting;
      case 'poison':
        return ImageType.poison;
      case 'ground':
        return ImageType.ground;
      case 'flying':
        return ImageType.flying;
      case 'psychic':
        return ImageType.psychic;
      case 'bug':
        return ImageType.bug;
      case 'rock':
        return ImageType.rock;
      case 'ghost':
        return ImageType.ghost;
      case 'dragon':
        return ImageType.dragon;
      case 'dark':
        return ImageType.dark;
      case 'steel':
        return ImageType.steel;
      case 'fairy':
        return ImageType.fairy;
      default:
        return ImageType.normal;
    }
  }


  static statsRename(String name) {
    switch(name.toLowerCase()) {
      case 'hp':
        return 'HP';
      case 'attack':
        return 'Attack';
      case 'defense':
        return 'Defense';
      case 'special-attack':
        return 'Sp. Atk';
      case 'special-defense':
        return 'Sp. Def';
      case 'speed':
        return 'Speed';
      default:
        return 'HP';
    }
  }


  static String capitalize(String s) => s[0].toUpperCase() + s.substring(1);


  toast({required String message}) async {
    print(message);
  }

  
 static void showCustomSnackBar(BuildContext context, {required String message, required String imagePath}) {
  final snackBar = SnackBar(
    backgroundColor: Colors.white,
    content: Container(
      margin: const EdgeInsets.only(left: 12, right: 20, bottom: 12),
      padding: const EdgeInsets.only(left: 5, right: 5, top: 10, bottom: 10),
      decoration: BoxDecoration(
        color: const Color.fromRGBO(249,249,249, 1.0),
        borderRadius: BorderRadius.circular(10),
        boxShadow: const [
          BoxShadow(
            color: Color.fromRGBO(255,214,0, 1.0),
            spreadRadius: 0,
            blurRadius: 3,
            offset: Offset(0, 0), // changes position of shadow
          ),
        ],
      ),
      child: Row(
        children: <Widget>[
          Image.asset(imagePath, height: 20, width: 20),
          const SizedBox(width: 10),
          Text(message, style: const TextStyle(color: Colors.black, fontSize: 16)),
        ],
      ),
    ),
  );
   ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

}