import 'package:flutter/material.dart';

class AppColors {
  static const white = Color(0xFFFFFFFF);
  static const green = Color(0xFF1B932C);
  static const greenLite = Color(0xFF70D090);
  static const black80 = Color(0xFF000000);
  static const purple = Color(0xFF8849B0);
  static const gray = Color(0xFF555252);
  static const grayMedium = Color(0xFF8A8886);
  static const grayLight = Color(0xFFD9D9D9);
  static const blue = Color(0xFF265DFC);
  static const red = Color(0xFFF10A34);
  static const yellow = Color(0xFFFDC000);
  static const backGroundPageDetail = Color(0xFFF7F7F7);
  static const blueDark = Color(0xFF040534);
  static const titleColor = Color(0xFF525252);
  static const descriptionColor = Color(0xFF4F4F4F);
}

class TypeColors {
  static const normal = Color(0xFF7D3600);
  static const fire = Color(0xFFF10A34);
  static const water = Color(0xFF265DFC);
  static const electric = Color(0xFFF8A801);
  static const grass = Color(0xFF1B932C);
  static const ice = Color(0xFF8FC3E9);
  static const fighting = Color(0xFF0F0905);
  static const poison = Color(0xFF8849B0);
  static const ground = Color(0xFF673E2C);
  static const flying = Color(0xFF9EC4DD);
  static const psychic = Color(0xFFF95587);
  static const bug = Color(0xFF54DC44);
  static const rock = Color(0xFF54473D);
  static const ghost = Color(0xFF8A8886);
  static const dragon = Color(0xFF0804B4);
  static const dark = Color(0xFF040534);
  static const steel = Color(0xFF5D666E);
  static const fairy = Color(0xFFFF48CC);
}


class CardColors {
  static const normal = Color(0xFFFFE0CA);
  static const fire = Color(0xFFEC8C4C);
  static const water = Color(0xFF20C5F5);
  static const electric = Color(0xFFFCF47C);
  static const grass = Color(0xFF70D090);
  static const ice = Color(0xFF8FC3E9);
  static const fighting = Color(0xFFB8B8B8);
  static const poison = Color(0xFFDDA1E7);
  static const ground = Color(0xFF9E6E53);
  static const flying = Color(0xFF9EC4DD);
  static const psychic = Color(0xFFA98DF8);
  static const bug = Color(0xFFD0EC94);
  static const rock = Color(0xFF9A8371);
  static const ghost = Color(0xFFCDCDCD);
  static const dragon = Color(0xFFB7DBFF);
  static const dark = Color(0xFF8D8ECB);
  static const steel = Color(0xFF7A95AA);
  static const fairy = Color(0xFFFDB7DA);
}


class CardColorsDark {
  static const normal = Color(0xFFe1bda4);
  static const fire = Color(0xFFaa602e);
  static const water = Color(0xFF1988a9);
  static const electric = Color(0xFFc5be5d);
  static const grass = Color(0xFF4d9064);
  static const ice = Color(0xFF74a0c0);
  static const fighting = Color(0xFF9c9b9b);
  static const poison = Color(0xFFbc89c5);
  static const ground = Color(0xFF8b6147);
  static const flying = Color(0xFF87a6ba);
  static const psychic = Color(0xFF957ed5);
  static const bug = Color(0xFFa8be79);
  static const rock = Color(0xFF827061);
  static const ghost = Color(0xFFb4b4b4);
  static const dragon = Color(0xFF9ebddd);
  static const dark = Color(0xFF7374a7);
  static const steel = Color(0xFF687f91);
  static const fairy = Color(0xFFc98dab);
}
