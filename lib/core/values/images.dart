class Images {
  static const String logo = 'assets/images/logo_pokemon.png';
  static const String pokeSample = 'assets/images/pokemon_sample.png';
  static const String iconPlant = 'assets/images/icon_plant.png';
  static const String iconFavoriteOf = 'assets/images/icon_favorite_of.png';
  static const String iconFavoriteOn = 'assets/images/icon_favorite_on.png';
  static const String iconPoison = 'assets/images/icon_poison.png';
  static const String bgTransparent = 'assets/images/bg_transp.png';
  static const String iconArrow = 'assets/images/icon_arrow.png';
  static const String iconSetting = 'assets/images/icon_setting.png';
  static const String iconClosed = 'assets/images/icon_closed.png';
  static const String loadBoll = 'assets/images/load_boll.gif';
  static const String iconNotification = 'assets/images/icon_notification.png';
  static const String linkedin = 'assets/images/in.png';
  static const String instagram = 'assets/images/instagram.png';
  static const String n = 'assets/images/n.png';
  static const String whatsapp = 'assets/images/whatsapp.png';
  static const String github = 'assets/images/github.png';
  static const String favorites = 'assets/images/favorites.svg';
  static const String home = 'assets/images/home.svg';
  static const String profile = 'assets/images/profile.svg';
  static const String iconHeight = 'assets/images/icon_height.png';
  static const String iconWeight = 'assets/images/icon_weight.png';
  static const String backButton = 'assets/images/backbutton.png';
}


class ImageType {
  static const String bug = 'assets/images/bug.png';
  static const String dark = 'assets/images/dark.png';
  static const String dragon = 'assets/images/dragon.png';
  static const String electric = 'assets/images/electric.png';
  static const String fairy = 'assets/images/fairy.png';
  static const String fighting = 'assets/images/fighting.png';
  static const String fire = 'assets/images/fire.png';
  static const String flying = 'assets/images/flying.png';
  static const String ghost = 'assets/images/ghost.png';
  static const String grass = 'assets/images/grass.png';
  static const String ground = 'assets/images/ground.png';
  static const String ice = 'assets/images/ice.png';
  static const String normal = 'assets/images/normal.png';
  static const String poison = 'assets/images/poison.png';
  static const String psychic = 'assets/images/psychic.png';
  static const String rock = 'assets/images/rock.png';
  static const String steel = 'assets/images/steel.png';
  static const String water = 'assets/images/water.png';
}