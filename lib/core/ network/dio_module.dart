import 'package:dio/dio.dart';

class DioModule {
  static Dio dio() {
    final options = BaseOptions(
      baseUrl: 'https://pokeapi.co/api/v2/',
      connectTimeout: const Duration(milliseconds: 5000),
      receiveTimeout: const Duration(milliseconds: 3000),
    );
    var dio = Dio(options);
    return dio;
  }
}