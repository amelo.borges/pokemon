class Failure {
  String? message;
  String? exceptionError;
  String? type;

  Failure({
    this.message,
    this.type,
    this.exceptionError,
  });
}