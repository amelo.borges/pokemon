import 'package:connectivity_plus/connectivity_plus.dart';

class Helper {
  static Future<bool> hasInternetConnection() async {
    var conectividadeResultado = await (Connectivity().checkConnectivity());
    if (conectividadeResultado == ConnectivityResult.mobile || conectividadeResultado == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
    }
  }
}