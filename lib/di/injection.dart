import 'package:get/get.dart';
import 'package:pokemon/data/repository/favorites_repository_impl.dart';
import 'package:pokemon/presentation/ui/detail/pokemon_detail_controller.dart';
import 'package:pokemon/presentation/ui/favorites/favorites_controller.dart';
import '../data/datasources/local/local_data_source.dart';
import '../data/datasources/local/local_data_source_impl.dart';
import '../data/datasources/remote/remote_data_source.dart';
import '../data/datasources/remote/remote_data_source_impl.dart';
import '../data/repository/home_repository_impl.dart';
import '../data/repository/pokemon_detail_repository_impl.dart';
import '../domain/repository/favorites_repository.dart';
import '../domain/repository/home_repository.dart';
import '../domain/repository/pokemon__detail_repository.dart';
import '../domain/use_cases/favorites_use_cases.dart';
import '../domain/use_cases/home_use_cases.dart';
import '../domain/use_cases/pokemon_detail_use_cases.dart';
import '../presentation/ui/home/home_controller.dart';

class Injection {
  Injection._internal();
  static final Injection _instance = Injection._internal();
  static Injection get instance => _instance;

  dependencies() {
    Get.lazyPut<RemoteDataSource>(()=> RemoteDataSourceImpl());
    Get.lazyPut<LocalDataSource>(()=> LocalDataSourceImpl());

    // FAVORITES
    Get.lazyPut<FavoritesRepository>(()=> FavoritesRepositoryImpl(local: Get.find()));
    Get.lazyPut<FavoritesUseCases>(()=> FavoritesUseCases(repository: Get.find()));
    Get.lazyPut<FavoritesController>(()=> FavoritesController(useCases: Get.find()));

    // DETAIL
    Get.lazyPut<PokemonDetailRepository>(()=> PokemonDetailRepositoryImpl(local: Get.find(), remote: Get.find()));
    Get.lazyPut<PokemonDetailUseCases>(()=> PokemonDetailUseCases(repository: Get.find()));
    Get.lazyPut<PokemonDetailController>(()=> PokemonDetailController(useCases: Get.find()));

    // HOME
    Get.lazyPut<HomeRepository>(()=> HomeRepositoryImpl(remote: Get.find(),local: Get.find()));
    Get.lazyPut<HomeUseCases>(()=> HomeUseCases(repository: Get.find()));
    Get.lazyPut<HomeController>(()=> HomeController(useCases: Get.find()));
  }

}