import 'package:either_dart/either.dart';
import 'package:get/get.dart';

import '../../presentation/navigation/routes.dart';
import '../entity/pokemon/pokemon_entity.dart';
import '../repository/home_repository.dart';

class HomeUseCases {

  final HomeRepository repository;
  HomeUseCases({required this.repository});

  Future<Either<dynamic, List<PokemonEntity>>> fetchPokemon({required int offset, required int limit}) async {
    return await repository.fetchPokemon(offset: offset, limit: limit);
  }

  goPokemonDetail(PokemonEntity object) async {
    await Get.toNamed(Routes.pokemonDetail, arguments: object);
  }

}