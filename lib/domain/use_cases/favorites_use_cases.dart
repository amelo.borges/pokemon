import 'package:either_dart/either.dart';
import 'package:get/get.dart';
import '../../core/error/failure.dart';
import '../../presentation/navigation/routes.dart';
import '../entity/pokemon/pokemon_entity.dart';
import '../repository/favorites_repository.dart';

class FavoritesUseCases{

  final FavoritesRepository repository;
  FavoritesUseCases({required this.repository});

  Future<Either<Failure, List<PokemonEntity>>>fetchPokemonFavorites() async {
    final response = await repository.fetchPokemonFavorites();
    return response;
  }

  goPokemonDetail(PokemonEntity object) async {
    await Get.toNamed(Routes.pokemonDetail, arguments: object);
  }

  Future<Either<Failure, bool>> pokemonFavorite({required int pokemonId}){
    return repository.pokemonFavorite(pokemonId: pokemonId);
  }

}