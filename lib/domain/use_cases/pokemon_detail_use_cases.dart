import 'package:either_dart/either.dart';

import '../../core/error/failure.dart';
import '../entity/evolution/evolution_entity.dart';
import '../entity/move/move_entity.dart';
import '../entity/pokemon/pokemon_entity.dart';
import '../repository/pokemon__detail_repository.dart';

class PokemonDetailUseCases{

  final PokemonDetailRepository repository;
  PokemonDetailUseCases({required this.repository});

  Future<Either<Failure, List<MoveEntity>>> fetchPokemonMove({required PokemonEntity pokemon}) async{
    return await repository.fetchPokemonMove(pokemon: pokemon);
  }

  Future<Either<Failure, List<EvolutionEntity>>> fetchPokemonEvolution({required PokemonEntity pokemon}) async{
    return await repository.fetchPokemonEvolution(pokemon: pokemon);
  }

  Future<Either<Failure, bool>> pokemonFavorite({required int pokemonId}){
    return repository.pokemonFavorite(pokemonId: pokemonId);
  }

}