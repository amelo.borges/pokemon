import 'package:either_dart/either.dart';
import 'package:pokemon/domain/entity/pokemon/pokemon_entity.dart';
import '../../core/error/failure.dart';

abstract class HomeRepository {
  Future<Either<Failure, List<PokemonEntity>>> fetchPokemon({required int offset,required int limit});
}