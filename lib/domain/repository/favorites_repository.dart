import 'package:either_dart/either.dart';
import '../../core/error/failure.dart';
import '../entity/pokemon/pokemon_entity.dart';

abstract class FavoritesRepository{
  Future<Either<Failure, List<PokemonEntity>>> fetchPokemonFavorites();
  Future<Either<Failure, bool>> pokemonFavorite({required int pokemonId});
}