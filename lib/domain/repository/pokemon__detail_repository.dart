import 'package:either_dart/either.dart';
import 'package:pokemon/domain/entity/evolution/evolution_entity.dart';
import 'package:pokemon/domain/entity/move/move_entity.dart';
import '../../core/error/failure.dart';
import '../entity/pokemon/pokemon_entity.dart';

abstract class PokemonDetailRepository{
  Future<Either<Failure, List<MoveEntity>>> fetchPokemonMove({required PokemonEntity pokemon});
  Future<Either<Failure, List<EvolutionEntity>>> fetchPokemonEvolution({required PokemonEntity pokemon});
  Future<Either<Failure, bool>> pokemonFavorite({required int pokemonId});
}