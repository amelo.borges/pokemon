
import 'package:equatable/equatable.dart';

class WeaknessEntity extends Equatable {
  String name;
  String url;

  WeaknessEntity({
    required this.name,
    required this.url
  });

  @override
  List<Object?> get props => [
    name,
    url
  ];
}