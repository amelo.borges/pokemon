import 'package:equatable/equatable.dart';

class StatsEntity extends Equatable {
  int baseStat;
  String name;
  String url;
  StatsEntity({
    required this.baseStat,
    required this.name,
    required this.url,
  });

  @override
  List<Object?> get props => [
    baseStat,
    name,
    url,
  ];
}