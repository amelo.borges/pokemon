import 'package:equatable/equatable.dart';

class StrenghtsEntity extends Equatable {
  String name;
  String url;

  StrenghtsEntity({
    required this.name,
    required this.url
  });

  @override
  List<Object?> get props => [
    name,
    url
  ];
}