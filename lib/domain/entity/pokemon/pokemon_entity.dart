import 'package:equatable/equatable.dart';

import 'stats_entity.dart';
import 'strenghts_entity.dart';
import 'type_entity.dart';
import 'weakness_entity.dart';


class PokemonEntity extends Equatable {
  int id;
  String name;
  String image;
  String color;
  bool isFavorite;
  int pageGroup;
  int baseExperience;
  List<TypeEntity> types;
  String height;
  List<String> abilities;
  String weight;
  List<WeaknessEntity> weakness;
  List<StrenghtsEntity> strenghts;
  List<StatsEntity> stats;

  PokemonEntity({
    required this.id,
    required this.name,
    required this.image,
    required this.types,
    required this.color,
    required this.pageGroup,
    required this.height,
    required this.abilities,
    required this.weight,
    required this.weakness,
    required this.strenghts,
    required this.stats,
    required this.isFavorite,
    required this.baseExperience,
  });

  @override
  List<Object?> get props => [
    id,
    name,
    image,
    types,
    color,
    isFavorite,
    pageGroup,
    height,
    abilities,
    weight,
    weakness,
    strenghts,
    baseExperience
  ];
}
