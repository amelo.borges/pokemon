import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class TypeEntity extends Equatable{
  String name;
  String url;
  String color;
  String image;

  TypeEntity({
    required this.name,
    required this.url,
    required this.color,
    required this.image,
  });

  @override
  List<Object?> get props => [
    name,
    url,
    color,
    image,
  ];
}