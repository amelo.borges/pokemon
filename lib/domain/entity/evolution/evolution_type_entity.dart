import 'package:equatable/equatable.dart';

class EvolutionTypeEntity extends Equatable{
  String name;
  String url;
  String type;

  EvolutionTypeEntity({
    required this.name,
    required this.url,
    required this.type,
  });

  @override
  List<Object?> get props => [
    name,
    url,
    type,
  ];
}