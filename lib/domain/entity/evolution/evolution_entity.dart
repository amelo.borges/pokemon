
import 'package:equatable/equatable.dart';

import 'evolution_type_entity.dart';

class EvolutionEntity extends Equatable {
  int id;
  String name;
  List<EvolutionTypeEntity> evolutionTypes;

  EvolutionEntity({
    required this.id,
    required this.name,
    required this.evolutionTypes,
  });

  @override
  List<Object?> get props => [
    id,
    name,
    evolutionTypes,
  ];
}