import 'package:equatable/equatable.dart';
import 'move_type_entity.dart';

class MoveEntity extends Equatable{
  int id;
  String name;
  List<MoveTypeEntity> moveTypes;

  MoveEntity(
  {
    required this.id,
    required this.name,
    required this.moveTypes,
  });

  @override
  List<Object?> get props => [
    id,
    name,
    moveTypes,
  ];
}