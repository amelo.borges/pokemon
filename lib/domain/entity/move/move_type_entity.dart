import 'package:equatable/equatable.dart';

class MoveTypeEntity extends Equatable{
  String move;
  String type;

  MoveTypeEntity({
    required this.move,
    required this.type,
  });

  @override
  List<Object?> get props => [
    move,
    type,
  ];
}