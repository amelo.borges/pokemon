import 'package:flutter/material.dart';
import '../../core/values/images.dart';

class AppBarCustom extends StatelessWidget {
  const AppBarCustom({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 80,
          padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.asset(Images.iconSetting,height: 28.0),
              Text("O")
            ],
          ),
        ),
        Image.asset(
          Images.logo,
          width: 145.0,
        ),
        const SizedBox(
          height: 35.0,
        ),
      ],
    );
  }
}
