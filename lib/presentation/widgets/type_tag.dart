import 'package:flutter/material.dart';
import '../../core/values/images.dart';

Widget buildTypeTag({
  required String tagName,
  required Color tagBackgroundColor,
  required String tagIcon,
}) {
  return Container(
    height: 22,
    padding: const EdgeInsets.symmetric(horizontal: 12),
    decoration: BoxDecoration(
      color: tagBackgroundColor,
      borderRadius: BorderRadius.circular(12.0),
    ),
    child: _buildRow(tagName, tagIcon),
  );
}

Row _buildRow(String tagName, String tagIcon) {
  return Row(
    children: [
      Image.asset(tagIcon, height: 12.0),
      const SizedBox(width: 4),
      Text(
        tagName,
        style: const TextStyle(
          color: Colors.white,
          fontSize: 12,
          fontWeight: FontWeight.bold,
        ),
      ),
    ],
  );
}