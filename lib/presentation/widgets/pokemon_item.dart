import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:pokemon/presentation/widgets/type_tag.dart';
import '../../core/utils/util.dart';
import '../../core/values/images.dart';
import '../../domain/entity/pokemon/pokemon_entity.dart';


class PokemonItem extends StatelessWidget {
  final PokemonEntity data;
  final Color backgroundColor;
  final Function(PokemonEntity) onTap;

  const PokemonItem({
    Key? key,
    required this.backgroundColor,
    required this.onTap,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onTap(data),
      child: _buildCard()
    );
  }

  Card _buildCard() {
    return Card(
      color: backgroundColor,
      elevation: 2.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(32.0),
      ),
      child: SizedBox(
        height: 130.0,
        child: LayoutBuilder(
          builder: (context, constraints) {
            return Stack(
              alignment: Alignment.centerRight,
              children: [
                Image.asset(Images.bgTransparent, height: constraints.maxHeight),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    children: [
                      Expanded(flex: 1,child: _buildPokemonInfo()),
                      _buildPokemonImage(constraints),
                      _buildFavoriteIcon(data.isFavorite ?? false)
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }


  Column _buildPokemonInfo() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("#${data.baseExperience}", style: const TextStyle(fontSize: 11.0)),
        Text(
          data.name,
          style: const TextStyle(
            fontSize: 28.0,
            fontWeight: FontWeight.w700,
          ),
        ),
        const SizedBox(height: 8.0),

        // TYPE TAGS
        Row(
          children: [
            for(final item in data.types)
            Padding(
              padding: const EdgeInsets.only(right: 3.0),
              child: buildTypeTag(
                tagName: item.name,
                tagBackgroundColor: Util.getColor(item.color),
                tagIcon: item.image,
              ),
            ),
          ],
        ),
      ],
    );
  }

  Align _buildPokemonImage(BoxConstraints constraints) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Hero(
        tag: data.image,
        child: CachedNetworkImage(
          height: constraints.maxHeight * 0.75,
          imageUrl: data.image,
          progressIndicatorBuilder: (context, url, downloadProgress) =>
              LoadingAnimationWidget.fourRotatingDots(
                color: Colors.white,
                size: 30,
              ),
          errorWidget: (context, url, error) => const Icon(Icons.error),
        ),
      ),
    );
  }


  Padding _buildFavoriteIcon(bool isFavorite) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0, right: 10.0),
      child: Align(
        alignment: Alignment.topRight,
        child: Image.asset(isFavorite ? Images.iconFavoriteOn : Images.iconFavoriteOf,
          width: 30.0,
        ),
      ),
    );
  }
}