import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import '../../../di/injection.dart';
import '../favorites/favorites_view.dart';
import '../home/home_view.dart';
import '../profile/profile_view.dart';


class HubController extends GetxController{

  List<Widget> pages = [
    HomeView(),
    FavoritesView(),
    ProfileView(),
  ];

  RxInt currentIndex = 0.obs;
  changePage(int value){
    Injection.instance.dependencies();
    currentIndex.value = value;
  }

  @override
  void onInit() {
    super.onInit();
  }

}