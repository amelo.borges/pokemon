import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pokemon/presentation/ui/hub/hub_controller.dart';

import '../../../core/values/images.dart';
import 'local_widgets/app_bar_custom.dart';

class HubView extends StatelessWidget {
  const HubView({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HubController>(
      builder: (ctrl) {
        return Obx(() {
          return Scaffold(
            body: Column(
              children: [
                const AppBarCustom(),
                Expanded(child: ctrl.pages[ctrl.currentIndex.value]),
              ],
            ),
            bottomNavigationBar: BottomNavigationBar(
              currentIndex: ctrl.currentIndex.value,
              onTap: ctrl.changePage,
              selectedItemColor: Colors.deepOrange, // Cor do item selecionado
              items: [
                BottomNavigationBarItem(
                  icon: SvgPicture.asset(
                    Images.home,
                    width: 20.0,
                    colorFilter: ctrl.currentIndex.value == 0 ? const ColorFilter.mode(Colors.deepOrange, BlendMode.srcIn) : ColorFilter.mode(Colors.black54, BlendMode.srcIn),
                  ),
                  label: 'Home',
                ),
                BottomNavigationBarItem(
                  icon: SvgPicture.asset(
                    Images.favorites,
                    width: 20.0,
                    colorFilter: ctrl.currentIndex.value == 1 ? const ColorFilter.mode(Colors.deepOrange, BlendMode.srcIn) : ColorFilter.mode(Colors.black54, BlendMode.srcIn),
                  ),
                  label: 'Favorites',
                ),
                BottomNavigationBarItem(
                  icon: SvgPicture.asset(
                    Images.profile,
                    width: 20.0,
                    colorFilter: ctrl.currentIndex.value == 2 ? const ColorFilter.mode(Colors.deepOrange, BlendMode.srcIn) : ColorFilter.mode(Colors.black54, BlendMode.srcIn),
                  ),
                  label: 'Favorites',
                ),
              ],
            ),
          );
        });
      },
    );
  }
}