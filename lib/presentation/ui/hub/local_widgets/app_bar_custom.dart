import 'package:flutter/material.dart';
import '../../../../core/values/images.dart';

class AppBarCustom extends StatelessWidget {
  const AppBarCustom({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 40.0),
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.asset(Images.iconSetting,height: 26.0),
              Image.asset(Images.iconNotification,height: 21.0),
            ],
          ),
          Hero(tag: "logo",child: Image.asset(Images.logo,width: 149)),
        ],
      ),
    );
  }
}
