import 'package:get/instance_manager.dart';

import 'hub_controller.dart';

class HubBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HubController());
  }
}