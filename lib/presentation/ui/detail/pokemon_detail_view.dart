import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../core/utils/util.dart';
import '../../../core/values/images.dart';
import '../../../domain/entity/pokemon/type_entity.dart';
import 'local_widget/tab_nav.dart';
import '../../widgets/type_tag.dart';
import 'local_widget/inclined_rectangle_painter.dart';
import 'pokemon_detail_controller.dart';

class PokemonDetailView extends StatelessWidget {

  const PokemonDetailView({
    Key? key,
  }) : super(key: key);

  final double pokemonImageHeight = 210.0;

  @override
  Widget build(BuildContext context) {
    final deviceHeight = MediaQuery
        .of(context)
        .size
        .height;
    return GetBuilder<PokemonDetailController>(
      builder: (ctrl) =>
          Obx(
                  () =>
                  Scaffold(
                    backgroundColor: Colors.white,
                    body: Material(
                      color: Colors.black.withOpacity(.3),
                      child: Padding(
                        padding: EdgeInsets.only(top: deviceHeight * .05),
                        child: Column(
                          children: [
                            Stack(
                              alignment: Alignment.topCenter,
                              children: [
                                Image.asset(
                                  Images.logo,
                                  height: deviceHeight * .076,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10.0, top: 20.0),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: InkWell(
                                      onTap: () => Get.back(),
                                      child: Image.asset(
                                        Images.backButton,
                                        height: 28,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Expanded(
                              child: Stack(
                                children: [
                                  _buildContentContainer(ctrl),
                                  _buildInclinedRectangle(ctrl),
                                  _buildPokemonInfo(ctrl),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
          ),
    );
  }

  Container _buildContentContainer(PokemonDetailController ctrl) {
    return Container(
      width: double.infinity,
      color: Colors.white,
      margin: EdgeInsets.only(top: pokemonImageHeight),
      child: Column(
        children: [
          const SizedBox(height: 20.0),
          TabNav(onTap: ctrl.changeTab),
          Expanded(child: ctrl.pages.elementAt(ctrl.selectedTab.value)),
        ],
      ),
    );
  }

  // Desenha o retângulo inclinado
  CustomPaint _buildInclinedRectangle(PokemonDetailController ctrl) {
    return CustomPaint(
      painter: InclinedRectanglePainter(beginColor: Util.getCardColor(
          typeItem: ctrl.pokemon.color, isDark: true),
          topColor: Util.getCardColor(typeItem: ctrl.pokemon.color)),
      child: Container(
        height: pokemonImageHeight,
        width: double.infinity,
        color: Colors.transparent,
      ),
    );
  }

  Container _buildPokemonInfo(PokemonDetailController ctrl) {
    return Container(
      height: pokemonImageHeight + 30.0,
      padding: const EdgeInsets.only(top: 20.0, left: 8.0),
      child: Row(
        children: [
          _buildPokemonImage(ctrl.pokemon.image),
          Expanded(child: _buildPokemonDetails(ctrl)),
        ],
      ),
    );
  }

  Align _buildPokemonImage(String image) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Hero(
        tag: image,
        child: CachedNetworkImage(
          height: pokemonImageHeight,
          imageUrl: image,
          progressIndicatorBuilder: (context, url, downloadProgress) =>
              CircularProgressIndicator(value: downloadProgress.progress),
          errorWidget: (context, url, error) => const Icon(Icons.error),
        ),
      ),
    );
  }

  Column _buildPokemonDetails(PokemonDetailController ctrl) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _buildFavoriteIcon(ctrl),
        const SizedBox(height: 48.0),
        _buildPokemonNumber(ctrl),
        _buildPokemonName(ctrl.pokemon.name),
        const SizedBox(height: 5.0),
        _buildPokemonTypes(ctrl.pokemon.types),
      ],
    );
  }

  Padding _buildFavoriteIcon(PokemonDetailController ctrl) {
    return Padding(
      padding: const EdgeInsets.only(top: 15.0, right: 20.0),
      child: Align(
        alignment: Alignment.topRight,
        child: InkWell(
          onTap: () {
            ctrl.pokemonFavorite(pokemonId: ctrl.pokemon.id);
          },
          child: Obx(() {
            return Image.asset(
              ctrl.isFavorite.value ? Images.iconFavoriteOn : Images
                  .iconFavoriteOf,
              width: 32.0,
            );
          }),
        ),
      ),
    );
  }

  Text _buildPokemonNumber(PokemonDetailController ctrl) {
    return  Text(
      "#${ctrl.pokemon.baseExperience}",
      style: const TextStyle(
        color: Colors.white,
        fontSize: 12.0,
      ),
    );
  }

  Text _buildPokemonName(String name) {
    return Text(
      name,
      style: const TextStyle(
        fontSize: 26.0,
        fontWeight: FontWeight.bold,
        color: Colors.white,
      ),
    );
  }

  Row _buildPokemonTypes(List<TypeEntity> types) {
    return Row(
      children: [
        for(final type in types)
          Padding(
            padding: const EdgeInsets.only(right: 4.0),
            child: buildTypeTag(
              tagName: type.name,
              tagBackgroundColor: Util.getColor(type.color),
              tagIcon: type.image,
            ),
          ),
      ],
    );
  }
}