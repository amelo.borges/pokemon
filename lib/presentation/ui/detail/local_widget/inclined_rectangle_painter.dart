import 'dart:math';
import 'package:flutter/material.dart';

class InclinedRectanglePainter extends CustomPainter {
  final Color beginColor;
  final Color topColor;
  InclinedRectanglePainter({required this.beginColor, required this.topColor});
  @override
  void paint(Canvas canvas, Size size) {

    // Define o retângulo para o gradiente
    final Rect rect = Rect.fromLTWH(0, 0, size.width, size.height);

    // Cria um gradiente linear
    Gradient gradient = LinearGradient(
      begin: Alignment.bottomCenter,
      end: Alignment.topCenter,
      colors: <Color>[beginColor, topColor],
      stops: [0.0, 1.0],
    );

    var paint = Paint()
      ..shader = gradient.createShader(rect)
      ..style = PaintingStyle.fill;

    var path = Path();

    // Define o raio do arredondamento
    double radius = 40.0;

    // Calcula a altura da inclinação baseada na largura da tela e no ângulo de 30 graus
    double inclinedHeight = tan(11 * pi / 180) * size.width;

    // Começa do canto inferior esquerdo
    path.moveTo(0, size.height);

    // Linha até o início do arredondamento superior esquerdo
    path.lineTo(0, inclinedHeight + radius);

    // Arco para borda arredondada superior esquerda
    path.arcToPoint(
      Offset(radius, inclinedHeight),
      radius: Radius.circular(radius),
      clockwise: true,
    );

    // Linha até o início do arredondamento superior direito
    path.lineTo(size.width - radius, 13);

    // Arco para borda arredondada superior direita
    path.arcToPoint(
      Offset(size.width, radius),
      radius: Radius.circular(radius),
    );

    // Linha até o canto inferior direito
    path.lineTo(size.width, size.height);

    // Fecha o caminho voltando ao início
    path.close();

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}