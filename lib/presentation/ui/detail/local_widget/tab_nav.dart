import 'package:flutter/material.dart';
import 'package:pokemon/core/values/colors.dart';

class TabNav extends StatefulWidget {
  final Function(int) onTap;
  const TabNav({
    Key? key,
    required this.onTap,
  }) : super(key: key);

  @override
  State<TabNav> createState() => _TabNavState();
}

class _TabNavState extends State<TabNav> {
  int _selectedIndex = 0;

  onTabTap(int index) {
    setState(() {
      widget.onTap(index);
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0, bottom: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          for (var i = 0; i < 4; i++)
            InkWell(
              onTap: () => onTabTap(i),
              child: LayoutBuilder(
                builder: (context, constraints) {
                  final label = ['About', 'Stats', 'Evolution', 'Moves'][i];
                  final isSelected = _selectedIndex == i;
                  final textWidget = Text(
                    label,
                    style: TextStyle(color: isSelected ? AppColors.red : Colors.black, fontWeight:  isSelected ? FontWeight.w600 : FontWeight.normal ),
                  );
                  final textWidth = _getTextWidth(label, textWidget.style!);

                  return Column(
                    children: [
                      // Label
                      textWidget,

                      // Barra indica que esta selecionado
                      AnimatedContainer(
                        duration: const Duration(milliseconds: 300),
                        height: 2.0,
                        width: isSelected ? textWidth : 0,
                        color: AppColors.red,
                      ),
                    ],
                  );
                },
              ),
            ),
        ],
      ),
    );
  }

  double _getTextWidth(String text, TextStyle style) {
    final textPainter = TextPainter(
      text: TextSpan(text: text, style: style),
      maxLines: 1,
      textDirection: TextDirection.ltr,
    )..layout(minWidth: 0, maxWidth: double.infinity);

    return textPainter.size.width;
  }
}