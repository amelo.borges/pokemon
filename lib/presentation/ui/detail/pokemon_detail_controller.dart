import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import '../../../core/utils/util.dart';
import '../../../core/values/images.dart';
import '../../../domain/entity/evolution/evolution_type_entity.dart';
import '../../../domain/entity/move/move_type_entity.dart';
import '../../../domain/entity/pokemon/pokemon_entity.dart';
import '../../../domain/use_cases/pokemon_detail_use_cases.dart';
import 'pages/about.dart';
import 'pages/evolution.dart';
import 'pages/moves.dart';
import 'pages/stats.dart';

class PokemonDetailController extends GetxController with Util {

  final PokemonDetailUseCases useCases;
  PokemonDetailController({required this.useCases});

  PokemonEntity pokemon = Get.arguments;

  RxBool isFavorite = false.obs;

  List<MoveTypeEntity> moves = [];
  RxBool isMovesLoading = false.obs;

  List<EvolutionTypeEntity> pokemonEvolutions = [];
  RxBool isEvolutionLoading = false.obs;

  List<Widget> pages = [
    const About(),
    const Stats(),
    const Evolution(),
    const Moves()
  ];


  Future fetchPokemonMove() async{
      isMovesLoading.value = true;
     final response = await useCases.fetchPokemonMove(pokemon: pokemon);
     if(response.isRight){
       moves = response.right.first.moveTypes;
     }else{
       final failure = response.left;
       loggerError(message: failure.message);
     }
      isMovesLoading.value = false;
  }


  Future fetchPokemonEvolution() async{
    isEvolutionLoading.value = true;
    final response = await useCases.fetchPokemonEvolution(pokemon: pokemon);
    if(response.isRight){
      pokemonEvolutions = response.right.first.evolutionTypes;
    }else{
      final failure = response.left;
      loggerError(message: failure.message);
    }
    isEvolutionLoading.value = false;
  }


  RxInt selectedTab = 0.obs;
  changeTab(int position){
    selectedTab.value = position;
  }


  Future pokemonFavorite({required int pokemonId}) async{
    final response = await useCases.pokemonFavorite(pokemonId: pokemonId);
    if(response.isRight){
      isFavorite.value = response.right;
      Util.showCustomSnackBar(Get.context!, message: isFavorite.value ? "Pokémon adicionado aos favoritos" : "Pokémon removido dos favoritos" ,  imagePath: isFavorite.value ? Images.iconFavoriteOn : Images.iconFavoriteOf);
    }
  }


  @override
  void onInit() {
    super.onInit();
    isFavorite.value = pokemon.isFavorite;
    fetchPokemonMove();
    fetchPokemonEvolution();
  }
}