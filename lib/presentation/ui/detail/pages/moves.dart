import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokemon/core/values/images.dart';
import '../../../../core/utils/util.dart';
import '../../../../core/values/colors.dart';
import '../pokemon_detail_controller.dart';

class Moves extends StatelessWidget {
  final int movesCount;
  final String moveName;

  const Moves({Key? key, this.movesCount = 40, this.moveName = "Tackle"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ctrl = Get.find<PokemonDetailController>();
    return Material(
      color: AppColors.backGroundPageDetail,
      child: Obx(() {
        return ctrl.isMovesLoading.value ? SizedBox(
            width: double.infinity,
            child: Center(child: Image.asset(Images.loadBoll, height: 50.0, width: 50.0))) : ListView.builder(
          shrinkWrap: true,
          itemCount: ctrl.moves.length,
          itemBuilder: (context, index) {
            final move = ctrl.moves[index];
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30.0),
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 15.0),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: AppColors.gray.withOpacity(.2),
                      width: 1.0,
                    ),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "${move.move.capitalizeFirst}",
                      style: const TextStyle(
                        color: AppColors.gray,
                        fontSize: 12.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    Container(
                      width: 18.0,
                      height: 18.0,
                      decoration: BoxDecoration(
                        color: Util.getColor(move.type),
                        borderRadius: BorderRadius.circular(90.0),
                      ),
                      child: Center(
                        child: Image.asset(Util.getImage(move.type),
                          width: 10.0,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
      }),
    );
  }
}