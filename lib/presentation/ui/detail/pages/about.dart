import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../core/utils/util.dart';
import '../../../../core/values/colors.dart';
import '../../../../core/values/images.dart';
import '../../../../domain/entity/pokemon/pokemon_entity.dart';
import '../../../widgets/type_tag.dart';
import '../pokemon_detail_controller.dart';

class About extends StatelessWidget {
  const About({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    PokemonEntity pokemon = Get.find<PokemonDetailController>().pokemon;
    return Material(
      color: AppColors.backGroundPageDetail,
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 40.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const Text(
                  "From the time it is born, a flame burns at the tip of its tail. Its life would end if the flame were to go out."),
              const SizedBox(height: 20.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _buildInfoColumn(
                      title: "Height",
                      value: "${pokemon.height} m",
                      icon: Images.iconHeight),
                  _buildInfoColumn(
                      title: "Weight",
                      value: "${pokemon.weight} kg",
                      icon: Images.iconWeight),
                  _buildInfoColumn(title: "Gender", value: "---"),
                ],
              ),
              const SizedBox(height: 15.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _buildInfoColumn(title: "Category", value: "Mouse"),
                  _buildInfoColumn(
                      title: "Abilities",
                      value: "${pokemon.abilities.join(', ')}"),
                  const SizedBox(width: 40.0),
                ],
              ),
              _buildTypeTagsRow("Weakenes",pokemon.weakness),
              //_buildTypeTagsRow("Strenghts",[]),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildInfoColumn({String? title, String? value, String? icon}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          title ?? "",
          style: const TextStyle(
            color: AppColors.gray,
            fontWeight: FontWeight.bold,
          ),
        ),
        Row(
          children: [
            if (icon != null)
              Image.asset(
                icon,
                width: 12.0,
              ),
            const SizedBox(width: 4.0),
            Text(
              value ?? "",
              style: const TextStyle(
                fontSize: 12.0,
                fontWeight: FontWeight.normal,
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildTypeTagsRow(String title,List<dynamic> items) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title,
              style: const TextStyle(fontSize: 18.0, fontWeight: FontWeight.w600),
          ),
          const SizedBox(height: 10.0),
          Wrap(
            children: items.map(
              (item) {
                return Padding(
                  padding: const EdgeInsets.only(right: 5.0, bottom: 5.0),
                  child: SizedBox(
                    width: Util.getTextWidth(item.name) + 38.0,
                    child: buildTypeTag(
                      tagName: item.name,
                      tagBackgroundColor: Util.getColor(item.name.toLowerCase()),
                      tagIcon: Util.getImage(item.name.toLowerCase()),
                    ),
                  ),
                );
              },
            ).toList(),
          ),
        ],
      ),
    );
  }
}
