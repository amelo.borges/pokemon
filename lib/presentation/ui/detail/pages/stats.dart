import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokemon/core/values/colors.dart';

import '../../../../core/utils/util.dart';
import '../../../../domain/entity/pokemon/pokemon_entity.dart';
import '../pokemon_detail_controller.dart';

class Stats extends StatelessWidget {
  const Stats({super.key});

  @override
  Widget build(BuildContext context) {
    PokemonEntity pokemon = Get.find<PokemonDetailController>().pokemon;
    int totalStats = pokemon.stats.fold(0, (prev, element) => prev + element.baseStat);
    return Material(
      color: AppColors.backGroundPageDetail,
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 30.0),
          child: Column(
            children: [
              ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: pokemon.stats.length,
                itemBuilder: (context, index) {
                  final item = pokemon.stats[index];
                  return _buildStatRow(item.name, item.baseStat.toString());
                },
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: _buildTotalStatRow("Total", totalStats),
              ),
            ],
          ),
        ),
      ),
    );
  }


    Widget _buildTotalStatRow(String title, int total) {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: Text(
              title,
              style: const TextStyle(
                color: AppColors.gray,
                fontSize: 12.0,
                fontWeight: FontWeight.w600,
              ),
              textAlign: TextAlign.end,
            ),
          ),
        ),
        Expanded(
          flex: 4,
          child: TweenAnimationBuilder<int>(
            tween: IntTween(begin: 0, end: total),
            duration: const Duration(milliseconds: 500),
            builder: (context, value, child) {
              return Text(
                value.toString(),
                style: const TextStyle(
                  color: AppColors.gray,
                  fontSize: 12.0,
                  fontWeight: FontWeight.w600,
                ),
              );
            },
          ),
        ),
      ],
    );
  }


  Widget _buildStatRow(String title, String value) {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: Padding(
            padding: const EdgeInsets.only(right: 16.0, bottom: 10.0),
            child: Text(
              Util.statsRename(title),
              style: const TextStyle(
                color: AppColors.gray,
                fontSize: 12.0,
                fontWeight: FontWeight.w600,
              ),
              textAlign: TextAlign.end,
            ),
          ),
        ),
        Expanded(
          flex: 4,
          child: Row(
            children: [
              Text(value,style: const TextStyle(
                color: AppColors.gray,
                fontSize: 12.0,
                fontWeight: FontWeight.w600,
              ),),
              const SizedBox(width: 10.0),
              Expanded(
                child: Stack(
                  children: [
                    Container(
                      height: 10.0,
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                    LayoutBuilder(
                      builder: (context, constraints) => TweenAnimationBuilder(
                        tween: Tween<double>(begin: 0, end: double.parse(value) / 100 * constraints.maxWidth),
                        duration: const Duration(milliseconds: 500),
                        builder: (context, double value2, child) {
                          return Container(
                            height: 10.0,
                            width: value2,
                            decoration: BoxDecoration(
                              color: double.parse(value) >= 90 ? Color(0xFF0804B4) : AppColors.red,
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}