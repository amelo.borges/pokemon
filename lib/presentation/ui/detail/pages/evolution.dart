import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/utils/util.dart';
import '../../../../core/values/colors.dart';
import '../../../../core/values/images.dart';
import '../../../widgets/type_tag.dart';
import '../pokemon_detail_controller.dart';

class Evolution extends StatelessWidget {
  const Evolution({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ctrl = Get.find<PokemonDetailController>();
    return Obx(
      () {
        return Material(
          color: AppColors.backGroundPageDetail,
          child: ctrl.isEvolutionLoading.value
              ? SizedBox(
                  width: double.infinity,
                  child: Center(
                      child: Image.asset(Images.loadBoll,
                          height: 50.0, width: 50.0)))
              : Padding(
                  padding: const EdgeInsets.only(top: 30.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      for (final item in ctrl.pokemonEvolutions)
                        PokemonColumn(
                          imageUrl: item.url,
                          name: item.name,
                          number: "#007",
                          tagName: item.type,
                          tagBackgroundColor: Util.getColor(item.type),
                          tagIcon: Util.getImage(item.type),
                          borderColor: Util.getColor(item.type),
                        ),
                    ],
                  ),
                ),
        );
      },
    );
  }
}

class PokemonColumn extends StatelessWidget {
  final String imageUrl;
  final String name;
  final String number;
  final String tagName;
  final Color tagBackgroundColor;
  final String tagIcon;
  final Color borderColor;

  const PokemonColumn({
    Key? key,
    required this.imageUrl,
    required this.name,
    required this.number,
    required this.tagName,
    required this.tagBackgroundColor,
    required this.tagIcon,
    required this.borderColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CachedNetworkImage(
          height: 117.0,
          width: 117.0,
          imageUrl: imageUrl,
          imageBuilder: (context, imageProvider) => Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(90.0),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: borderColor,
                  spreadRadius: 1,
                  blurRadius: 1,
                  offset: const Offset(0, 0), // changes position of shadow
                ),
              ],
              image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,
              ),
            ),
          ),
          placeholder: (context, url) => const CircularProgressIndicator(),
          errorWidget: (context, url, error) => const Icon(Icons.error),
        ),
        const SizedBox(height: 15.0),
        Row(
          children: [
            Text(
              name,
              style: const TextStyle(
                color: AppColors.gray,
                fontSize: 13.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(width: 6.0),
            Text(number, style: const TextStyle(fontSize: 8.0))
          ],
        ),
        const SizedBox(height: 10.0),
        buildTypeTag(
          tagName: tagName,
          tagBackgroundColor: tagBackgroundColor,
          tagIcon: tagIcon,
        ),
      ],
    );
  }
}
