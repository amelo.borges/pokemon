import 'package:get/get.dart';
import '../../../data/datasources/local/local_data_source.dart';
import '../../../data/datasources/local/local_data_source_impl.dart';
import '../../../data/datasources/remote/remote_data_source.dart';
import '../../../data/datasources/remote/remote_data_source_impl.dart';
import '../../../data/repository/pokemon_detail_repository_impl.dart';
import '../../../domain/repository/pokemon__detail_repository.dart';
import '../../../domain/use_cases/pokemon_detail_use_cases.dart';
import 'pokemon_detail_controller.dart';

class PokemonDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RemoteDataSource>(()=> RemoteDataSourceImpl());
    Get.lazyPut<LocalDataSource>(()=> LocalDataSourceImpl());

    // DETAIL
    Get.lazyPut<PokemonDetailRepository>(()=> PokemonDetailRepositoryImpl(local: Get.find(), remote: Get.find()));
    Get.lazyPut<PokemonDetailUseCases>(()=> PokemonDetailUseCases(repository: Get.find()));
    Get.lazyPut<PokemonDetailController>(()=> PokemonDetailController(useCases: Get.find()));
  }
}
