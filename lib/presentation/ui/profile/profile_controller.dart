import 'package:get/get.dart';

import '../../../core/values/images.dart';

class ProfileController extends GetxController{


  final List<Map<String, dynamic>> socialMedia = [
    {
      'name': 'LinkedIn',
      'icon': Images.linkedin,
      'url': ''
    },
    {
      'name': 'Instagram',
      'icon': Images.instagram,
      'url': ''
    },
    {
      'name': 'N',
      'icon': Images.n,
      'url': ''
    },
    {
      'name': 'GitHub',
      'icon': Images.github,
      'url': ''
    },
    {
      'name': 'WhatsApp',
      'icon': Images.whatsapp,
      'url': ''
    }
  ];


  @override
  void onInit() {
    super.onInit();
  }

}