import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokemon/core/values/colors.dart';
import 'package:pokemon/presentation/ui/profile/profile_controller.dart';

class ProfileView extends StatelessWidget {
  const ProfileView({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProfileController>(
      init: ProfileController(),
      assignId: true,
      builder: (ctrl) {
        return SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(bottom: 30.0),
            child: Column(
              children: [
                const SizedBox(height: 30.0),
                _buildAvatar(),
                const SizedBox(height: 20.0),
                const Text("Alexandre Borges",
                    style: TextStyle(
                        fontSize: 32.0,
                        color: AppColors.blueDark,
                        fontWeight: FontWeight.w600)),
                const Text("Mobile Developer",
                    style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w500,
                        color: AppColors.grayMedium)),
                const SizedBox(height: 30.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _buildBtn(
                        text: "Follow",
                        onTap: () {},
                        backgroundColor: AppColors.blueDark,
                        textColor: Colors.white),
                    const SizedBox(width: 20.0),
                    _buildBtn(
                        text: "Message",
                        onTap: () {},
                        backgroundColor: AppColors.grayLight,
                        textColor: AppColors.blueDark),
                  ],
                ),
                const SizedBox(height: 50.0),
                _buildInfo(
                    title: "Sobre min",
                    description:
                        "Com mais de uma década de experiência em desenvolvimento mobile, sou especializado em aplicativos Android nativos usando Java e Kotlin, e possuo cinco anos de experiência com Flutter para soluções multiplataforma. Profundo conhecedor do Firebase, com oito anos de experiência em suas diversas ferramentas como Firestore e Storage. Habilidades avançadas em arquiteturas de software como MVVM e Clean Architecture, além de experiência na integração de APIs de inteligência artificial, como OpenAI, complementam meu perfil, permitindo o desenvolvimento de soluções escaláveis e inovadoras."),
                const SizedBox(height: 30.0),
                _buildInfo(
                    title: "Sobre o projeto",
                    description: "Protótipo realizado para Guarani Sistemas para o teste para a vaga de Desenvolvedor Flutter Pleno."),
                const SizedBox(height: 30.0),
                _buildSocialMedia(ctrl.socialMedia),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildAvatar() {
    return Stack(
      alignment: Alignment.center,
  children: [
    Container(
      width: 110.0,
      height: 110.0,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(90.0),
        boxShadow: const [
          BoxShadow(
            color: AppColors.yellow,
            spreadRadius: 4,
            blurRadius: 1,
            offset: Offset(0, 0),
          ),
        ],
      ),
    ),
    Container(
      width: 104.0,
      height: 104.0,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(90.0),
        boxShadow: const [
          BoxShadow(
            color: AppColors.blue,
            spreadRadius: 4,
            blurRadius: 1,
            offset: Offset(0, 0),
          ),
        ],
      ),
      child: CachedNetworkImage(
        imageUrl:
            "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/1.png",
        imageBuilder: (context, imageProvider) => Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(90.0),
            image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
          ),
        ),
        placeholder: (context, url) => const CircularProgressIndicator(),
        errorWidget: (context, url, error) => Icon(Icons.error),
      ),
    ),
  ],
);
  }

  Widget _buildBtn(
      {required String text,
      required Color textColor,
      required Function() onTap,
      required Color backgroundColor}) {
    return SizedBox(
      width: 120.0,
      height: 35.0,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: backgroundColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(32.0),
          ),
        ),
        onPressed: onTap,
        child: Text(
          text,
          style: TextStyle(
            color: textColor,
            fontSize: 16.0,
            fontWeight: FontWeight.normal,
          ),
        ),
      ),
    );
  }

  Widget _buildInfo({
    required String title,
    required String description,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Text(
              title,
              style: const TextStyle(
                fontSize: 16.0,
                color: AppColors.blueDark,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(24.0),
              color: AppColors.grayLight.withOpacity(0.2),
            ),
            child: Text(
              description,
              style: const TextStyle(fontSize: 13.0),
            ),
          ),
        ],
      ),
    );
  }


  Widget _buildSocialMedia(List<Map<String, dynamic>> socials) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(24.0),
          color: AppColors.grayLight.withOpacity(0.2),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            for(final item in socials)
            Image.asset(
              item["icon"],
              width: 32.0,
              height: 32.0,
            ),
          ],
        ),
      )
    );
  }
}
