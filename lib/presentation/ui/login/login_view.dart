import 'package:flutter/material.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokemon/presentation/ui/login/login_controller.dart';

class LoginView extends StatelessWidget {

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginController>(builder: (ctrl) {
      return Scaffold(
        body: Center(
          child: Padding(
            padding: EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // Logo
                const FlutterLogo(size: 100),
                const SizedBox(height: 50),
                // Campo de email
                TextField(
                  controller: emailController,
                  decoration: const InputDecoration(
                    labelText: 'Email',
                    border: OutlineInputBorder(),
                  ),
                ),
                const SizedBox(height: 20),
                // Campo de senha
                TextField(
                  controller: passwordController,
                  decoration: const InputDecoration(
                    labelText: 'Senha',
                    border: OutlineInputBorder(),
                  ),
                  obscureText: true,
                ),
                const SizedBox(height: 20),
                // Botão para entrar
                ElevatedButton(
                  child: const Text('Entrar'),
                  onPressed: () {
                    // Aqui você pode adicionar a lógica para lidar com o login
                    print('Email: ${emailController.text}');
                    print('Senha: ${passwordController.text}');
                  },
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}