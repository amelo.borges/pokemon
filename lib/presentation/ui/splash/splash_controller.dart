import 'package:get/get.dart';

import '../../navigation/routes.dart';

class SplashController extends GetxController {

  init() async {
    await Future.delayed(const Duration(seconds: 3));
    Get.offNamed(Routes.hub);
  }


  @override
  void onInit() {
    super.onInit();
    init();
  }

}