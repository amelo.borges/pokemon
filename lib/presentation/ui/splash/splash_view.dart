import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokemon/presentation/ui/splash/splash_controller.dart';

import '../../../core/values/images.dart';

class SplashView extends StatelessWidget {
  const SplashView({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SplashController>(
      builder: (ctrl) {
        return Scaffold(
          backgroundColor: const Color.fromRGBO(46, 68, 149, 1.0),
          body: Center(
            child: Hero(tag: "logo",child: Image.asset(Images.logo)),
          ),
        );
      },
    );
  }
}
