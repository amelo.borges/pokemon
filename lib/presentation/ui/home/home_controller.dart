import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:pokemon/domain/entity/pokemon/pokemon_entity.dart';
import '../../../core/utils/util.dart';
import '../../../core/values/images.dart';
import '../../../domain/use_cases/home_use_cases.dart';


class HomeController extends GetxController with Util {
  final HomeUseCases useCases;
  HomeController({required this.useCases});

  static const _pageSize = 10;
  final PagingController<int, PokemonEntity> pagingController = PagingController(firstPageKey: 0);

  @override
  void onInit() {
    super.onInit();
    pagingController.addPageRequestListener((pageKey) {
      fetchPokemon(pageKey);
    });
  }

  Future fetchPokemon(int pageKey) async {
    final response = await useCases.fetchPokemon(offset: pageKey, limit: _pageSize);
    if(response.isRight){
      final isLastPage = response.right.length < _pageSize;
      if (isLastPage) {
        pagingController.appendLastPage(response.right);
      } else {
        final nextPageKey = pageKey + response.right.length;
        pagingController.appendPage(response.right, nextPageKey);
      }
    } else {
      pagingController.error = response.left;
    }
  }

  @override
  void dispose() {
    pagingController.dispose();
    super.dispose();
  }

  goToPokemonDetails(PokemonEntity object) async {
    await useCases.goPokemonDetail(object);
    // Não é mais necessário chamar fetchPokemon aqui.
  }
}