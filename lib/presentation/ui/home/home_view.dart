import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import '../../../core/utils/util.dart';
import '../../../core/values/images.dart';
import '../../../domain/entity/pokemon/pokemon_entity.dart';
import '../../widgets/pokemon_item.dart';
import 'home_controller.dart';

class HomeView extends StatelessWidget {
  const HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (ctrl) {
        return _buildHomeContent(ctrl);
      },
    );
  }

  Widget _buildHomeContent(HomeController ctrl) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, top: 30.0, right: 16.0),
      child: Column(
        children: [
          const Text(
            "Qual Pokémon você está procurando?",
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 29,
              height: 0.0,
            ),
          ),
          _buildPokemonList(ctrl),
        ],
      ),
    );
  }

  Widget _buildPokemonList(HomeController ctrl) {
    return Expanded(
      child: PagedListView<int, PokemonEntity>(
        pagingController: ctrl.pagingController,
        builderDelegate: PagedChildBuilderDelegate<PokemonEntity>(
          firstPageProgressIndicatorBuilder: (_) => _buildLoading(50.0),
          itemBuilder: (context, item, index) => _buildPokemonItem(context, ctrl, item),
          newPageProgressIndicatorBuilder: (_) => Padding(
            padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
            child: _buildLoading(40.0),
          ),
        ),
      ),
    );
  }

  Widget _buildPokemonItem(BuildContext context, HomeController ctrl, PokemonEntity item) {
    try {
      return PokemonItem(
        data: item,
        backgroundColor: Util.getCardColor(typeItem: item.color, isDark: false),
        onTap: (object) => ctrl.goToPokemonDetails(object),
      );
    } catch (e) {
      return const Text('Erro ao carregar o item.');
    }
  }

  Widget _buildLoading(double size) {
    return SizedBox(
        width: double.infinity,
        child: Center(child: Image.asset(Images.loadBoll, height: size, width: size)));
  }
}
