import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../core/utils/util.dart';
import '../../../core/values/images.dart';
import '../../../domain/entity/pokemon/pokemon_entity.dart';
import '../../../domain/use_cases/favorites_use_cases.dart';

class FavoritesController extends GetxController with Util {

  final FavoritesUseCases useCases;
  FavoritesController({required this.useCases});

  List<PokemonEntity> pokemon = [];
  RxBool pokemonLoading = false.obs;

  Future fetchPokemonFavorites() async {
    pokemonLoading.value = true;
    final response = await useCases.fetchPokemonFavorites();
    loggerWarning(message: response);
    if(response.isRight){
      pokemon = response.right;
    }else{
      pokemon = [];
    }
    pokemonLoading.value = false;
  }


  goToPokemonDetails(PokemonEntity object) async {
    await useCases.goPokemonDetail(object);
    fetchPokemonFavorites();
  }


  Future pokemonFavorite({required int pokemonId}) async{
    await useCases.pokemonFavorite(pokemonId: pokemonId);
     Util.showCustomSnackBar(Get.context!, message: "Pokémon removido dos favoritos", imagePath: Images.iconFavoriteOf);
    fetchPokemonFavorites();
  }


  @override
  void onInit() {
    super.onInit();
    fetchPokemonFavorites();
  }

}
