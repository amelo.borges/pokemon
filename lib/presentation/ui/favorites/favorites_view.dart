import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:pokemon/presentation/ui/favorites/favorites_controller.dart';

import '../../../core/utils/util.dart';
import '../../../core/values/colors.dart';
import '../../../core/values/images.dart';
import '../../widgets/pokemon_item.dart';

class FavoritesView extends StatelessWidget {
  const FavoritesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FavoritesController>(
      builder: (ctrl) => _buildFavoritesContent(ctrl),
    );
  }

  Widget _buildFavoritesContent(FavoritesController ctrl) {
    return Padding(
      padding: const EdgeInsets.only(top: 30.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Padding(
            padding: EdgeInsets.only(left: 16.0, bottom: 16.0),
            child: Text(
              "Meus Pokémons \nfavoritos",
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 30,
              ),
            ),
          ),
          _buildPokemonList(ctrl)
        ],
      ),
    );
  }

  Widget _buildPokemonList(FavoritesController ctrl) {
    return Obx(
      () => Expanded(
        child: ctrl.pokemonLoading.value
            ? Center(
                child: Image.asset(Images.loadBoll, height: 50.0, width: 50.0))
            : ctrl.pokemon.isNotEmpty
                ? ListView.builder(
                    shrinkWrap: true,
                    itemCount: ctrl.pokemon.length,
                    itemBuilder: (context, index) =>
                        _buildPokemonItem(context, ctrl, index),
                  )
                : const Center(
                    child: Text(
                      "Você ainda não tem nenhum pokémon favorito.",
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.w500,
                        color: AppColors.blueDark,
                      ),
                    ),
                  ),
      ),
    );
  }

  Widget _buildPokemonItem(
      BuildContext context, FavoritesController ctrl, int index) {
    try {
      final item = ctrl.pokemon[index];
      final device = MediaQuery.of(context);
      final controller = ScrollMotion();
      return Slidable(
        key: ValueKey(index),
        endActionPane: ActionPane(
          extentRatio: .28,
          motion: controller,
          children: [
            InkWell(
              onTap: () => ctrl.pokemonFavorite(pokemonId: item.id),
              child: Container(
                width: device.size.width * .25,
                height: 93,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(32.0),
                  color: Colors.red.withOpacity(.75),
                ),
                child: const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.delete_outline,
                      size: 34,
                      color: Colors.white,
                    ),
                    Text(
                      "Remover",
                      style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w400,
                        color: Colors.white,
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 16.0, right: 16.0),
          child: PokemonItem(
            data: item,
            backgroundColor: Util.getCardColor(typeItem: item.color),
            onTap: (object) => ctrl.goToPokemonDetails(object),
          ),
        ),
      );
    } catch (e) {
      return const Text('Erro ao carregar o item.');
    }
  }
}