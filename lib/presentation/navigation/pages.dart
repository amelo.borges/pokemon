import 'package:get/get.dart';
import 'package:pokemon/presentation/ui/detail/pokemon_detail_view.dart';
import 'package:pokemon/presentation/ui/login/login_binding.dart';
import 'package:pokemon/presentation/ui/splash/splash_view.dart';

import '../ui/detail/pokemon_detail_binding.dart';
import '../ui/hub/hub_binding.dart';
import '../ui/hub/hub_view.dart';
import '../ui/login/login_view.dart';
import '../ui/splash/splash_binding.dart';
import 'routes.dart';

class Pages {
  static final List<GetPage> items = [
    GetPage(
      name: Routes.hub,
      binding: HubBinding(),
      page: () => const HubView(),
    ),
    GetPage(
      transition: Transition.downToUp,
      name: Routes.pokemonDetail,
      binding: PokemonDetailBinding(),
      page: () => const PokemonDetailView(),
    ),
    GetPage(
      name: Routes.splash,
      binding: SplashBinding(),
      page: () => const SplashView(),
    ),
    GetPage(
      name: Routes.login,
      binding: LoginBinding(),
      page: () => LoginView(),
    )
  ];
}
