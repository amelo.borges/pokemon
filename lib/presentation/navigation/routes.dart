class Routes {
  static const String splash = '/';
  static const String hub = '/hub';
  static const String pokemonDetail = '/pokemonDetail';
  static const String login = '/login';
}